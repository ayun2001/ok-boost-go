package logger

import (
	"io"
	"log"
	"os"

	"gitee.com/ayun2001/ok-boost-go/async/logger/writer"
	"gitee.com/ayun2001/ok-boost-go/pkg/codec/json"
	"github.com/go-logr/logr"
	"github.com/go-logr/zapr"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var enccfg = zapcore.EncoderConfig{
	TimeKey:             "time",
	LevelKey:            "level",
	NameKey:             "logger",
	CallerKey:           "caller",
	FunctionKey:         zapcore.OmitKey,
	MessageKey:          "message",
	StacktraceKey:       "stacktrace",
	LineEnding:          zapcore.DefaultLineEnding,
	EncodeLevel:         zapcore.CapitalLevelEncoder, // 日志级别使用大写显示
	EncodeTime:          zapcore.ISO8601TimeEncoder,  // 自定义输出时间格式
	EncodeDuration:      zapcore.StringDurationEncoder,
	EncodeCaller:        zapcore.ShortCallerEncoder,
	NewReflectedEncoder: jsonReflectedEncoder,
}

// 根据 build 参数定义使用那种 json 解析器
func jsonReflectedEncoder(w io.Writer) zapcore.ReflectedEncoder {
	enc := json.NewEncoder(w)
	// For consistency with our custom JSON encoder.
	enc.SetEscapeHTML(false)
	return enc
}

func NewZapLogger(ws zapcore.WriteSyncer, opts ...zap.Option) *zap.Logger {
	if ws == nil {
		ws = zapcore.AddSync(os.Stdout)
	}

	core := zapcore.NewCore(
		zapcore.NewJSONEncoder(enccfg), // 构建 json 解码器
		ws,
		zap.NewAtomicLevelAt(zap.DebugLevel), // 将日志级别设置为 DEBUG
	)

	return zap.New(core, zap.AddCaller()).WithOptions(opts...)
}

func ZapLoggerToLogrStandardLogger(l *zap.Logger) logr.Logger {
	return zapr.NewLogger(l)
}

func ZapLoggerToSugaredLogger(l *zap.Logger) *zap.SugaredLogger {
	zap.ReplaceGlobals(l)
	return l.Sugar()
}

func ZapLoggerToStandardLogger(l *zap.Logger) *log.Logger {
	return zap.NewStdLog(l)
}

func NewZapConsoleBufferedWriter() *writer.Writer {
	return writer.NewBufferedWriter(os.Stdout, 0, writer.NewDefaultFeatureOpts())
}

func NewZapWriteSyncer(w io.Writer) zapcore.WriteSyncer {
	return zapcore.AddSync(w)
}

func NewZapAsyncConsoleLogger(wr *writer.Writer) *zap.Logger {
	return NewZapLogger(NewZapWriteSyncer(wr))
}

func NewZapConsoleLogger() *zap.Logger {
	return NewZapLogger(nil)
}
