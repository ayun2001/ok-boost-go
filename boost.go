package ok_boost_go

import (
	"gitee.com/ayun2001/ok-boost-go/logger"
	"github.com/gin-gonic/gin"
)

var (
	ZapLogger = logger.NewZapLogger(nil).Named("default")
	Logger    = logger.ZapLoggerToSugaredLogger(ZapLogger)
	StdLogger = logger.ZapLoggerToStandardLogger(ZapLogger)
)

func IsReleaseMode() bool {
	return gin.Mode() == gin.ReleaseMode
}

func SetReleaseMode() {
	gin.SetMode(gin.ReleaseMode)
}

func SetDebugMode() {
	gin.SetMode(gin.DebugMode)
}
