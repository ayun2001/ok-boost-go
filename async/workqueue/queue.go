package workqueue

import (
	"errors"
	"time"

	boost "gitee.com/ayun2001/ok-boost-go"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/panjf2000/ants/v2"
	"go.uber.org/zap"
	iwq "k8s.io/client-go/util/workqueue"
)

var (
	NeedRetrySubmitMessageError = errors.New("need retry submit message immediately")
)

type QueueWorkerFunc func(interface{}) error

type QueueConf struct {
	QueueCap   uint32             `json:"queueCap,omitempty" yaml:"queueCap,omitempty"`     // 队列容量
	WorkersNum uint16             `json:"workersNum,omitempty" yaml:"workersNum,omitempty"` // 工作协程数量
	Logger     *zap.SugaredLogger `json:"-" yaml:"-"`
}

type Queue struct {
	*workerQueue
	q         iwq.Interface
	queueConf *QueueConf
	workFunc  QueueWorkerFunc
}

func NewDefaultQueueConfig() *QueueConf {
	return &QueueConf{
		QueueCap:   defaultQueueCap,
		WorkersNum: defaultWorkerCount,
	}
}

func validQueueConfig(conf *QueueConf) *QueueConf {
	if conf != nil {
		if conf.QueueCap <= 0 {
			conf.QueueCap = defaultQueueCap
		}
		if conf.WorkersNum <= 0 || int32(conf.WorkersNum) > defaultMaxWorkerCount {
			conf.WorkersNum = defaultWorkerCount
		}
		if conf.Logger == nil {
			conf.Logger = boost.Logger
		}
	} else {
		conf = NewDefaultQueueConfig()
	}

	return conf
}

func NewQueue(queueConf *QueueConf, featureOpts *FeatureOpts, workFunc QueueWorkerFunc) (*Queue, error) {
	if workFunc == nil {
		return nil, WorkFuncIsNullError
	}

	queueConf = validQueueConfig(queueConf)
	featureOpts = validFeatureOpts(featureOpts)

	id := utils.GetRandIdString()
	queue := Queue{
		queueConf:   queueConf,
		workFunc:    workFunc,
		workerQueue: newWorkerQueue(id, featureOpts),
		q:           iwq.NewNamed(id),
	}

	// 启动 metrics
	if queue.featureOpts.EnableMetrics {
		queue.queueMetrics.registerMetrics()
	}

	return &queue, nil
}

func (q *Queue) Start() {
	// 启动 worker pool
	q.wg.Add(int(q.queueConf.WorkersNum))
	for i := uint16(0); i < q.queueConf.WorkersNum; i++ {
		go q.executor()
	}
}

// Close 安全关闭消息队列
func (q *Queue) Stop() {
	q.closeOnce.Do(func() {
		q.stopCancel()
		q.q.ShutDown()
		q.wg.Wait()
		if q.featureOpts.EnableMetrics {
			q.queueMetrics.unRegisterMetrics()
		}
		q.timer.Stop()
	})
}

func (q *Queue) executor() {
	defer q.wg.Done()

	for {
		// 读取消息体, 如果关闭就退出 for 循环。 如果没有关闭，没消消息，就会拥塞
		data, shutdown := q.q.Get()
		// 如果关闭就退出 for 循环
		if shutdown {
			return
		}

		if data == nil {
			q.queueConf.Logger.Warnw("executor get nil data", "id", q.id)
			continue
		}

		// 数据转换
		wd := data.(*workerData)

		// 处理 metrics
		if q.featureOpts.EnableMetrics {
			q.queueMetrics.cacheEventsNumber.WithLabelValues(utils.ApplicationName).Dec()
			q.queueMetrics.cacheDuration.WithLabelValues(utils.ApplicationName).Observe(float64(q.timer.Now() - wd.updateAt))
		}

		// 处理时间
		wd.updateAt = q.timer.Now()

		// 数据处理
		q.do(wd)
	}
}

func (q *Queue) do(data *workerData) {
	// 数据处理
	err := q.workFunc(data.value)
	q.q.Done(data.value) // 无论成功失败都要释放资源，告诉 Queue 数据我已经处理完毕了
	if err != nil && !q.q.ShuttingDown() {
		switch err {
		case NeedRetrySubmitMessageError:
			if err = q.Submit(data.value); err != nil {
				q.queueConf.Logger.Errorw("[Submit] workFunc retry exec failed", "id", q.id, "error", err)

			}
		default:
			q.queueConf.Logger.Errorw("workFunc exec failed", "id", q.id, "error", err)
		}
	}

	// 处理 metrics
	if q.featureOpts.EnableMetrics {
		q.queueMetrics.workerDuration.WithLabelValues(utils.ApplicationName).Observe(float64(q.timer.Now() - data.updateAt))
	}
}

func (q *Queue) Submit(value interface{}) error {
	// 如果队列关闭，直接返回
	if q.q.ShuttingDown() {
		return QueueIsClosedError
	}

	// 如果 value 为空，直接返回
	if value == nil {
		return SubmitMessageIsNullError
	}

	// 如果队列满了，直接返回
	if q.q.Len() >= int(q.queueConf.QueueCap) && q.queueConf.QueueCap > 0 {
		if q.featureOpts.EnableMetrics {
			q.queueMetrics.dropEventsCount.WithLabelValues(utils.ApplicationName).Inc()
		}
		return QueueIsFullError
	}

	// 如果队列没有满，就投递数据
	wd := workerData{}
	wd.value = value
	wd.updateAt = q.timer.Now()
	q.q.Add(&wd)

	// 处理 metrics
	if q.featureOpts.EnableMetrics {
		q.queueMetrics.cacheEventsNumber.WithLabelValues(utils.ApplicationName).Inc()
	}

	return nil
}

type QueueWithPool struct {
	Queue
	workerPool *ants.PoolWithFunc
}

func NewQueueWithPool(queueConf *QueueConf, featureOpts *FeatureOpts, workFunc QueueWorkerFunc) (*QueueWithPool, error) {
	if workFunc == nil {
		return nil, WorkFuncIsNullError
	}

	queueConf = validQueueConfig(queueConf)
	featureOpts = validFeatureOpts(featureOpts)

	// 创建 Queue
	q, err := NewQueue(queueConf, featureOpts, workFunc)
	if err != nil {
		return nil, err
	}

	// 创建 QueueWithPool
	queue := QueueWithPool{Queue: *q}

	// 启动 worker pool
	queue.workerPool, err = ants.NewPoolWithFunc(int(queueConf.WorkersNum), queue.handleWrapper, ants.WithOptions(ants.Options{
		ExpiryDuration: workerPoolStatusScanInterval * 3 * time.Second, // 每隔15秒自动回收休眠的 go routine
		PreAlloc:       true,
		Nonblocking:    false,
		Logger:         boost.StdLogger,
	}))
	if err != nil {
		return nil, err
	}

	return &queue, nil
}

func (q *QueueWithPool) Start() {
	// 启动 worker pool
	q.wg.Add(defaultWorkerCount)
	for i := uint16(0); i < defaultWorkerCount; i++ {
		go q.executor()
	}

	// 启动 guarder
	q.wg.Add(1)
	go q.guarder()
}

func (q *QueueWithPool) Stop() {
	q.closeOnce.Do(func() {
		q.stopCancel()
		q.q.ShutDown()
		q.wg.Wait()
		q.workerPool.Release()
		if q.featureOpts.EnableMetrics {
			q.queueMetrics.unRegisterMetrics()
		}
		q.timer.Stop()
	})
}

func (q *QueueWithPool) guarder() {
	ticker := time.NewTicker(time.Millisecond * workerPoolStatusScanInterval)

	defer func() {
		ticker.Stop()
		q.wg.Done()
	}()

	for {
		select {
		case <-ticker.C:
			if q.featureOpts.EnableMetrics {
				q.queueMetrics.workerRunningNumber.WithLabelValues(utils.ApplicationName).Set(float64(q.workerPool.Running()))
				q.queueMetrics.workerWaitingNumber.WithLabelValues(utils.ApplicationName).Set(float64(q.workerPool.Waiting()))
			}
			break
		case <-q.stopCtx.Done(): // 如果 stopCtx 被取消，结束循环
			return
		}
	}
}

func (q *QueueWithPool) executor() {
	defer q.wg.Done()

	for {
		// 读取消息体, 如果关闭就退出 for 循环。 如果没有关闭，没消消息，就会拥塞
		data, shutdown := q.q.Get()
		// 如果关闭就退出 for 循环
		if shutdown {
			return
		}

		if data == nil {
			q.queueConf.Logger.Warnw("executor get nil data", "id", q.id)
			continue
		}

		// 数据转换
		wd := data.(*workerData)

		// 处理 metrics
		if q.featureOpts.EnableMetrics {
			q.queueMetrics.cacheEventsNumber.WithLabelValues(utils.ApplicationName).Dec()
			q.queueMetrics.cacheDuration.WithLabelValues(utils.ApplicationName).Observe(float64(q.timer.Now() - wd.updateAt))
		}

		// 处理时间
		wd.updateAt = q.timer.Now()

		// 重新投递数据到 worker pool
		if err := q.workerPool.Invoke(wd); err != nil {
			q.queueConf.Logger.Errorw("executor invoke failed", "id", q.id, "error", err)
		}
	}
}

func (q *QueueWithPool) handleWrapper(data interface{}) {
	// 数据处理
	wd := data.(*workerData)
	q.do(wd)
}
