package workqueue

import (
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/prometheus/client_golang/prometheus"
)

type queueMetrics struct {
	dropEventsCount     *prometheus.CounterVec
	cacheEventsNumber   *prometheus.GaugeVec
	cacheDuration       *prometheus.HistogramVec
	workerDuration      *prometheus.HistogramVec
	workerRunningNumber *prometheus.GaugeVec
	workerWaitingNumber *prometheus.GaugeVec
}

func newWorkQueueMetrics() *queueMetrics {
	return &queueMetrics{}
}

func (m *queueMetrics) registerMetrics() {
	var metricLabels = []string{"app"}
	var id = utils.GetRandIdString()

	m.dropEventsCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "work_queue_discarded_events_count",
			Help:        "The total number of discarded events in work queue",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	m.cacheEventsNumber = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "work_queue_cached_events_number",
			Help:        "The current number of cached events in work queue",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	m.cacheDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "work_queue_cache_store_latency",
			Help:        "Cache events latencies (seconds) in work queue",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	m.workerDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "work_queue_worker_exec_latency",
			Help:        "Worker processing events latencies (seconds) in work queue",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	m.workerRunningNumber = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "work_queue_running_workers_number",
			Help:        "The current number of running workers in work queue",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	m.workerWaitingNumber = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "work_queue_waiting_workers_number",
			Help:        "The current number of waiting workers in work queue",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	prometheus.MustRegister(
		m.dropEventsCount, m.cacheEventsNumber,
		m.cacheDuration, m.workerDuration,
		m.workerRunningNumber, m.workerWaitingNumber,
	)
}

func (m *queueMetrics) unRegisterMetrics() {
	prometheus.Unregister(m.dropEventsCount)
	prometheus.Unregister(m.cacheEventsNumber)
	prometheus.Unregister(m.cacheDuration)
	prometheus.Unregister(m.workerDuration)
	prometheus.Unregister(m.workerRunningNumber)
	prometheus.Unregister(m.workerWaitingNumber)
}
