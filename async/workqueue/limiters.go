package workqueue

import (
	"time"

	"golang.org/x/time/rate"
	iwq "k8s.io/client-go/util/workqueue"
)

type (
	// 3种队列的接口
	Interface             = iwq.Interface
	DelayingInterface     = iwq.DelayingInterface
	RateLimitingInterface = iwq.RateLimitingInterface

	// ratelimiter 结构体
	RateLimiter                       = iwq.RateLimiter
	BucketRateLimiter                 = iwq.BucketRateLimiter
	ItemExponentialFailureRateLimiter = iwq.ItemExponentialFailureRateLimiter
	MaxOfRateLimiter                  = iwq.MaxOfRateLimiter
	ItemFastSlowRateLimiter           = iwq.ItemFastSlowRateLimiter
	WithMaxWaitRateLimiter            = iwq.WithMaxWaitRateLimiter
)

func DefaultControllerRateLimiter() RateLimiter {
	return iwq.DefaultControllerRateLimiter()
}

func DefaultBucketRateLimiter() RateLimiter {
	return &iwq.BucketRateLimiter{Limiter: rate.NewLimiter(rate.Limit(10), 100)}
}

func DefaultItemBasedRateLimiter() RateLimiter {
	return iwq.DefaultItemBasedRateLimiter()
}

func NewBucketRateLimiter(limit, burst uint32) RateLimiter {
	return &iwq.BucketRateLimiter{Limiter: rate.NewLimiter(rate.Limit(limit), int(burst))}
}

func NewItemExponentialFailureRateLimiter(baseDelay time.Duration, maxDelay time.Duration) RateLimiter {
	return iwq.NewItemExponentialFailureRateLimiter(baseDelay, maxDelay)
}

func NewItemFastSlowRateLimiter(fastDelay, slowDelay time.Duration, maxFastAttempts int) RateLimiter {
	return iwq.NewItemFastSlowRateLimiter(fastDelay, slowDelay, maxFastAttempts)
}

func NewMaxOfRateLimiter(limiters ...RateLimiter) RateLimiter {
	return iwq.NewMaxOfRateLimiter(limiters...)
}

func NewWithMaxWaitRateLimiter(limiter RateLimiter, maxDelay time.Duration) RateLimiter {
	return iwq.NewWithMaxWaitRateLimiter(limiter, maxDelay)
}
