package workqueue

import (
	"context"
	"errors"
	"math"
	"sync"
	"time"

	"gitee.com/ayun2001/ok-boost-go/pkg/timer"
)

const (
	workerPoolStatusScanInterval = 5000
	defaultWorkerCount           = 4
	defaultMaxWorkerCount        = math.MaxInt32 // ants 代码中有限制，这边也要做对应的限制
	defaultQueueCap              = math.MaxUint16 * 8
)

var (
	QueueIsClosedError       = errors.New("queue is closed")
	QueueIsFullError         = errors.New("queue is full")
	SubmitMessageIsNullError = errors.New("submit message is null")
	WorkFuncIsNullError      = errors.New("workFunc is null")
)

type workerQueue struct {
	id           string
	wg           sync.WaitGroup
	closeOnce    sync.Once
	stopCtx      context.Context
	stopCancel   context.CancelFunc
	featureOpts  *FeatureOpts
	queueMetrics *queueMetrics
	timer        *timer.CachedTimer
}

func newWorkerQueue(id string, featureOpts *FeatureOpts) *workerQueue {
	queue := workerQueue{
		id:           id,
		wg:           sync.WaitGroup{},
		closeOnce:    sync.Once{},
		featureOpts:  featureOpts,
		queueMetrics: newWorkQueueMetrics(),
		timer:        timer.NewCachedTimer(),
	}

	queue.stopCtx, queue.stopCancel = context.WithCancel(context.Background())

	return &queue
}

type ExecFuncs interface {
	Submit(interface{}) error
	SubmitAfter(interface{}, time.Duration) error
}

type workerData struct {
	updateAt int64
	value    interface{}
}

type FeatureOpts struct {
	EnableMetrics bool `json:"enableMetrics,omitempty" yaml:"enableMetrics,omitempty"` // 开启 metrics 记录
}

func NewDefaultFeatureOpts() *FeatureOpts {
	return &FeatureOpts{
		EnableMetrics: true,
	}
}

func validFeatureOpts(opts *FeatureOpts) *FeatureOpts {
	if opts != nil {
		return opts
	}

	return NewDefaultFeatureOpts()
}
