package workqueue

import (
	"errors"
	"math"
	"time"

	boost "gitee.com/ayun2001/ok-boost-go"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/panjf2000/ants/v2"
	"go.uber.org/zap"
	iwq "k8s.io/client-go/util/workqueue"
)

var (
	NeedRetrySubmitMessageCountError = errors.New("need retry submit message count")
)

const MaxMessageRetryCount = math.MaxInt64 - 1

type RateLimitingQueueWorkerFunc func(interface{}) (error, int64)

type RateLimitingQueueConf struct {
	QueueCap   uint32             `json:"queueCap,omitempty" yaml:"queueCap,omitempty"`     // 队列容量
	WorkersNum uint16             `json:"workersNum,omitempty" yaml:"workersNum,omitempty"` // 工作协程数量
	Logger     *zap.SugaredLogger `json:"-" yaml:"-"`
}

type RateLimitingQueue struct {
	*workerQueue
	rlq       iwq.RateLimitingInterface
	queueConf *RateLimitingQueueConf
	workFunc  RateLimitingQueueWorkerFunc
}

func NewDefaultRateLimitingQueueConfig() *RateLimitingQueueConf {
	return &RateLimitingQueueConf{
		QueueCap:   defaultQueueCap,
		WorkersNum: defaultWorkerCount,
	}
}

func validRateLimitingQueueConfig(conf *RateLimitingQueueConf) *RateLimitingQueueConf {
	if conf != nil {
		if conf.QueueCap <= 0 {
			conf.QueueCap = defaultQueueCap
		}
		if conf.WorkersNum <= 0 || int32(conf.WorkersNum) > defaultMaxWorkerCount {
			conf.WorkersNum = defaultWorkerCount
		}
		if conf.Logger == nil {
			conf.Logger = boost.Logger
		}
	} else {
		conf = NewDefaultRateLimitingQueueConfig()
	}

	return conf
}

func NewRateLimitingQueue(
	queueConf *RateLimitingQueueConf, featureOpts *FeatureOpts,
	rl RateLimiter, workFunc RateLimitingQueueWorkerFunc,
) (*RateLimitingQueue, error) {
	if workFunc == nil {
		return nil, WorkFuncIsNullError
	}

	if rl == nil {
		rl = iwq.DefaultControllerRateLimiter()
	}

	queueConf = validRateLimitingQueueConfig(queueConf)
	featureOpts = validFeatureOpts(featureOpts)

	id := utils.GetRandIdString()
	queue := RateLimitingQueue{
		queueConf:   queueConf,
		workFunc:    workFunc,
		workerQueue: newWorkerQueue(id, featureOpts),
		rlq:         iwq.NewNamedRateLimitingQueue(rl, id),
	}

	// 启动 metrics
	if queue.featureOpts.EnableMetrics {
		queue.queueMetrics.registerMetrics()
	}

	return &queue, nil
}

func (q *RateLimitingQueue) Start() {
	// 启动 worker pool
	q.wg.Add(int(q.queueConf.WorkersNum))
	for i := uint16(0); i < q.queueConf.WorkersNum; i++ {
		go q.executor()
	}
}

// Close 安全关闭消息队列
func (q *RateLimitingQueue) Stop() {
	q.closeOnce.Do(func() {
		q.stopCancel()
		q.rlq.ShutDown()
		q.wg.Wait()
		if q.featureOpts.EnableMetrics {
			q.queueMetrics.unRegisterMetrics()
		}
		q.timer.Stop()
	})
}

func (q *RateLimitingQueue) executor() {
	defer q.wg.Done()

	for {
		// 读取消息体, 如果关闭就退出 for 循环。 如果没有关闭，没消消息，就会拥塞
		data, shutdown := q.rlq.Get()
		// 如果关闭就退出 for 循环
		if shutdown {
			return
		}

		if data == nil {
			q.queueConf.Logger.Warnw("executor get nil data", "id", q.id)
			continue
		}

		// 数据转换
		wd := data.(*workerData)

		// 处理 metrics
		if q.featureOpts.EnableMetrics {
			q.queueMetrics.cacheEventsNumber.WithLabelValues(utils.ApplicationName).Dec()
			q.queueMetrics.cacheDuration.WithLabelValues(utils.ApplicationName).Observe(float64(q.timer.Now() - wd.updateAt))
		}

		// 处理时间
		wd.updateAt = q.timer.Now()

		// 数据处理
		q.do(wd)
	}
}

func (q *RateLimitingQueue) do(data *workerData) {
	// 数据处理
	err, number := q.workFunc(data.value)
	q.rlq.Done(data.value) // 无论成功失败都要释放资源，告诉 Queue 数据我已经处理完毕了
	if err != nil && !q.rlq.ShuttingDown() {
		switch err {
		case NeedRetrySubmitMessageDelayError:
			if number <= 0 {
				if err = q.Submit(data.value); err != nil {
					q.queueConf.Logger.Errorw("[Submit] workFunc retry exec failed", "id", q.id, "error", err)
				}
			} else {
				if err = q.SubmitAfter(data.value, utils.Int64ToTimeDuration(number)); err != nil {
					q.queueConf.Logger.Errorw("[SubmitAfter] workFunc retry exec failed", "id", q.id, "error", err)
				}
			}
		case NeedRetrySubmitMessageError:
			if err = q.Submit(data.value); err != nil {
				q.queueConf.Logger.Errorw("[Submit] workFunc retry exec failed", "id", q.id, "error", err)
			}
		case NeedRetrySubmitMessageCountError:
			retries := q.rlq.NumRequeues(data.value)
			if int64(retries) < number && number > 0 && number < math.MaxInt64 {
				if err = q.SubmitRateLimited(data.value); err != nil {
					q.queueConf.Logger.Errorw("[SubmitRateLimited] workFunc retry exec failed", "id", q.id, "error", err)
				}
			} else {
				// 如果重试次数超过了限制，就直接丢弃
				q.rlq.Forget(data.value)
				// 处理 metrics
				if q.featureOpts.EnableMetrics {
					q.queueMetrics.dropEventsCount.WithLabelValues(utils.ApplicationName).Inc()
				}
			}
		default:
			q.queueConf.Logger.Errorw("workFunc exec failed", "id", q.id, "error", err)
		}
	}

	// 处理 metrics
	if q.featureOpts.EnableMetrics {
		q.queueMetrics.workerDuration.WithLabelValues(utils.ApplicationName).Observe(float64(q.timer.Now() - data.updateAt))
	}
}

func (q *RateLimitingQueue) submit(value interface{}, delay time.Duration, rl bool) error {
	// 如果队列关闭，直接返回
	if q.rlq.ShuttingDown() {
		return QueueIsClosedError
	}

	// 如果 value 为空，直接返回
	if value == nil {
		return SubmitMessageIsNullError
	}

	// 如果队列满了，直接返回
	if q.rlq.Len() >= int(q.queueConf.QueueCap) && q.queueConf.QueueCap > 0 {
		if q.featureOpts.EnableMetrics {
			q.queueMetrics.dropEventsCount.WithLabelValues(utils.ApplicationName).Inc()
		}
		return QueueIsFullError
	}

	// 如果队列没有满，就投递数据
	wd := workerData{}
	wd.value = value
	wd.updateAt = q.timer.Now()
	if delay <= 0 {
		if rl {
			q.rlq.AddRateLimited(&wd)
		} else {
			q.rlq.Add(&wd)
		}
	} else {
		q.rlq.AddAfter(&wd, delay)
	}

	// 处理 metrics
	if q.featureOpts.EnableMetrics {
		q.queueMetrics.cacheEventsNumber.WithLabelValues(utils.ApplicationName).Inc()
	}

	return nil
}

func (q *RateLimitingQueue) Submit(value interface{}) error {
	return q.submit(value, 0, false)
}

func (q *RateLimitingQueue) SubmitAfter(value interface{}, delay time.Duration) error {
	return q.submit(value, delay, false)
}

func (q *RateLimitingQueue) SubmitRateLimited(value interface{}) error {
	return q.submit(value, 0, true)
}

type RateLimitingQueueWithPool struct {
	RateLimitingQueue
	workerPool *ants.PoolWithFunc
}

func NewRateLimitingQueueWithPool(
	queueConf *RateLimitingQueueConf, featureOpts *FeatureOpts,
	rl RateLimiter, workFunc RateLimitingQueueWorkerFunc,
) (*RateLimitingQueueWithPool, error) {
	if workFunc == nil {
		return nil, WorkFuncIsNullError
	}

	if rl == nil {
		rl = iwq.DefaultControllerRateLimiter()
	}

	queueConf = validRateLimitingQueueConfig(queueConf)
	featureOpts = validFeatureOpts(featureOpts)

	// 创建 Queue
	q, err := NewRateLimitingQueue(queueConf, featureOpts, rl, workFunc)
	if err != nil {
		return nil, err
	}

	// 创建 RateLimitingQueueWithPool
	queue := RateLimitingQueueWithPool{RateLimitingQueue: *q}

	// 启动 worker pool
	queue.workerPool, err = ants.NewPoolWithFunc(int(queueConf.WorkersNum), queue.handleWrapper, ants.WithOptions(ants.Options{
		ExpiryDuration: workerPoolStatusScanInterval * 3 * time.Second, // 每隔15秒自动回收休眠的 go routine
		PreAlloc:       true,
		Nonblocking:    false,
		Logger:         boost.StdLogger,
	}))
	if err != nil {
		return nil, err
	}

	return &queue, nil
}

func (q *RateLimitingQueueWithPool) Start() {
	// 启动 worker pool
	q.wg.Add(defaultWorkerCount)
	for i := uint16(0); i < defaultWorkerCount; i++ {
		go q.executor()
	}

	// 启动 guarder
	q.wg.Add(1)
	go q.guarder()
}

func (q *RateLimitingQueueWithPool) Stop() {
	q.closeOnce.Do(func() {
		q.stopCancel()
		q.rlq.ShutDown()
		q.wg.Wait()
		q.workerPool.Release()
		if q.featureOpts.EnableMetrics {
			q.queueMetrics.unRegisterMetrics()
		}
		q.timer.Stop()
	})
}

func (q *RateLimitingQueueWithPool) guarder() {
	ticker := time.NewTicker(time.Millisecond * workerPoolStatusScanInterval)

	defer func() {
		ticker.Stop()
		q.wg.Done()
	}()

	for {
		select {
		case <-ticker.C:
			if q.featureOpts.EnableMetrics {
				q.queueMetrics.workerRunningNumber.WithLabelValues(utils.ApplicationName).Set(float64(q.workerPool.Running()))
				q.queueMetrics.workerWaitingNumber.WithLabelValues(utils.ApplicationName).Set(float64(q.workerPool.Waiting()))
			}
			break
		case <-q.stopCtx.Done(): // 如果 stopCtx 被取消，结束循环
			return
		}
	}
}

func (q *RateLimitingQueueWithPool) executor() {
	defer q.wg.Done()

	for {
		// 读取消息体, 如果关闭就退出 for 循环。 如果没有关闭，没消消息，就会拥塞
		data, shutdown := q.rlq.Get()
		// 如果关闭就退出 for 循环
		if shutdown {
			return
		}

		if data == nil {
			q.queueConf.Logger.Warnw("executor get nil data", "id", q.id)
			continue
		}

		// 数据转换
		wd := data.(*workerData)

		// 处理 metrics
		if q.featureOpts.EnableMetrics {
			q.queueMetrics.cacheEventsNumber.WithLabelValues(utils.ApplicationName).Dec()
			q.queueMetrics.cacheDuration.WithLabelValues(utils.ApplicationName).Observe(float64(q.timer.Now() - wd.updateAt))
		}

		// 处理时间
		wd.updateAt = q.timer.Now()

		// 重新投递数据到 worker pool
		if err := q.workerPool.Invoke(wd); err != nil {
			q.queueConf.Logger.Errorw("executor invoke failed", "id", q.id, "error", err)
		}
	}
}

func (q *RateLimitingQueueWithPool) handleWrapper(data interface{}) {
	// 数据处理
	wd := data.(*workerData)
	q.do(wd)
}
