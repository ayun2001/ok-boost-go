package writer

import (
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/prometheus/client_golang/prometheus"
)

type writerMetrics struct {
	dropEventsCount   *prometheus.CounterVec
	cacheEventsNumber *prometheus.GaugeVec
	cacheDuration     *prometheus.HistogramVec
	workerDuration    *prometheus.HistogramVec
}

func newWriterQueueMetrics() *writerMetrics {
	return &writerMetrics{}
}

func (m *writerMetrics) registerMetrics() {
	var metricLabels = []string{"app"}
	var id = utils.GetRandIdString()

	m.dropEventsCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "buff_writer_discarded_events_count",
			Help:        "The total number of discarded message in buffer writer",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	m.cacheEventsNumber = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "buff_writer_cached_events_number",
			Help:        "The current number of cached message in buffer writer",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	m.cacheDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "buff_writer_cache_latency",
			Help:        "Cache messages latencies (seconds) in buffer writer",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	m.workerDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "buff_writer_worker_latency",
			Help:        "Worker processing messages latencies (seconds) in buffer writer",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	prometheus.MustRegister(m.dropEventsCount, m.cacheEventsNumber, m.cacheDuration, m.workerDuration)
}

func (m *writerMetrics) unRegisterMetrics() {
	prometheus.Unregister(m.dropEventsCount)
	prometheus.Unregister(m.cacheEventsNumber)
	prometheus.Unregister(m.cacheDuration)
	prometheus.Unregister(m.workerDuration)
}
