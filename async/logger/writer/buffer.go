package writer

import (
	"bytes"
	"sync"
)

const defaultBufferSize = 2048

type extraBuffer struct {
	buff     *bytes.Buffer
	updateAt int64
}

type extraBufferPool struct {
	bp sync.Pool
}

func newExtraBufferPool(size uint32) *extraBufferPool {
	if size <= 0 {
		size = defaultBufferSize
	}
	return &extraBufferPool{bp: sync.Pool{
		New: func() interface{} {
			return &extraBuffer{
				buff:     bytes.NewBuffer(make([]byte, 0, size)),
				updateAt: 0,
			}
		},
	}}
}

func (p *extraBufferPool) Get() *extraBuffer {
	return p.bp.Get().(*extraBuffer)
}

func (p *extraBufferPool) Put(b *extraBuffer) {
	if b != nil {
		b.updateAt = 0
		b.buff.Reset()
		p.bp.Put(b)
	}
}
