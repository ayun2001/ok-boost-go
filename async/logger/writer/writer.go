package writer

import (
	"bufio"
	"context"
	"errors"
	"io"
	"log"
	"math"
	"os"
	"sync"
	"sync/atomic"
	"time"

	"gitee.com/ayun2001/ok-boost-go/pkg/timer"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
)

const (
	defaultQueueCap       = math.MaxUint16 * 16
	defaultBufferedIoSize = 512 * 1024 // 512k
)

var (
	QueueIsClosedError    = errors.New("queue is closed")
	QueueIsFullError      = errors.New("queue is full")
	DropWriteMessageError = errors.New("message writing failure and drop it")
)

type Writer struct {
	id             string
	bufferPool     *extraBufferPool
	writer         io.Writer
	bufferIoWriter *bufio.Writer
	isClosed       atomic.Bool
	closeOnce      sync.Once
	stopCtx        context.Context
	stopCancel     context.CancelFunc
	wg             sync.WaitGroup
	queue          chan *extraBuffer
	timer          *timer.CachedTimer
	bufferIoLock   sync.Mutex
	writerMetrics  *writerMetrics
	featureOpts    *FeatureOpts
}

type FeatureOpts struct {
	EnableMetrics          bool `json:"enableMetrics,omitempty" yaml:"enableMetrics,omitempty"` // 开启 metrics 记录
	EnableBufferedIoWriter bool `json:"enableBufferedIoWriter,omitempty" yaml:"enableBufferedIoWriter,omitempty"`
}

func NewDefaultFeatureOpts() *FeatureOpts {
	return &FeatureOpts{
		EnableMetrics:          true,
		EnableBufferedIoWriter: true,
	}
}

func NewBufferedWriter(w io.Writer, queueCap uint32, featureOpts *FeatureOpts) *Writer {
	if queueCap <= 0 {
		queueCap = defaultQueueCap
	}

	if w == nil {
		w = os.Stdout
	}

	featureOpts = validFeatureOpts(featureOpts)

	wr := Writer{
		id:             utils.GetRandIdString(),
		bufferPool:     newExtraBufferPool(defaultBufferSize),
		writer:         w,
		bufferIoWriter: &bufio.Writer{},
		closeOnce:      sync.Once{},
		isClosed:       atomic.Bool{},
		wg:             sync.WaitGroup{},
		queue:          make(chan *extraBuffer, queueCap),
		timer:          timer.NewCachedTimer(),
		bufferIoLock:   sync.Mutex{},
		writerMetrics:  newWriterQueueMetrics(),
		featureOpts:    featureOpts,
	}

	wr.stopCtx, wr.stopCancel = context.WithCancel(context.Background())

	if wr.featureOpts.EnableMetrics {
		wr.writerMetrics.registerMetrics()
	}

	if featureOpts.EnableBufferedIoWriter {
		// 创建 writer
		wr.bufferIoWriter = bufio.NewWriterSize(w, defaultBufferedIoSize)
		wr.writer = wr.bufferIoWriter
		// 启动 goroutie
		wr.wg.Add(1)
		go wr.bufferIoWriteFlusher()

	}

	wr.wg.Add(1)
	go wr.poller()

	wr.isClosed.Store(false)

	return &wr
}

func (w *Writer) Write(p []byte) (int, error) {
	if w.isClosed.Load() {
		return 0, QueueIsClosedError
	}

	app := utils.ApplicationName

	b := w.bufferPool.Get()
	count, err := b.buff.Write(p)
	if err != nil {
		w.bufferPool.Put(b)
		if w.featureOpts.EnableMetrics {
			w.writerMetrics.dropEventsCount.WithLabelValues(app).Inc()
		}
		return count, err
	}

	b.updateAt = w.timer.Now() // 写入当前时间

	select {
	case w.queue <- b:
		if w.featureOpts.EnableMetrics {
			w.writerMetrics.cacheEventsNumber.WithLabelValues(app).Inc()
		}
		break
	default:
		w.bufferPool.Put(b)
		if w.featureOpts.EnableMetrics {
			w.writerMetrics.dropEventsCount.WithLabelValues(app).Inc()
		}
		return count, QueueIsFullError
	}

	return len(p), nil
}

func (w *Writer) Stop() {
	w.closeOnce.Do(func() {
		w.isClosed.Store(true)
		w.stopCancel()
		close(w.queue)
		w.wg.Wait()
		// 结束前将缓冲的内容全部写入
		if w.featureOpts.EnableBufferedIoWriter {
			w.bufferIoLock.Lock()
			_ = w.bufferIoWriter.Flush()
			w.bufferIoLock.Unlock()
		}
		if w.featureOpts.EnableMetrics {
			w.writerMetrics.unRegisterMetrics()
		}
		w.timer.Stop()
	})
}

func (w *Writer) poller() {
	var (
		eb         *extraBuffer
		err        error
		outChannel int64
		app        = utils.ApplicationName
	)

	defer w.wg.Done()

	for eb = range w.queue {
		// 时间处理
		outChannel = eb.updateAt
		eb.updateAt = w.timer.Now()
		// 处理 metrics
		if w.featureOpts.EnableMetrics {
			w.writerMetrics.cacheEventsNumber.WithLabelValues(app).Dec()
			w.writerMetrics.cacheDuration.WithLabelValues(app).Observe(float64(w.timer.Now() - outChannel))
		}
		// 写入消息体
		// _, err = w.writer.Write(eb.buff.Bytes())
		_, err = w.writeBytes(eb.buff.Bytes())
		if err != nil {
			if w.featureOpts.EnableMetrics {
				w.writerMetrics.dropEventsCount.WithLabelValues(app).Inc()
			}
			log.Printf("data write error, id: %s, error: %s, message: %s", w.id, err.Error(), utils.BytesToString(eb.buff.Bytes()))
		}
		// 处理 metrics
		if w.featureOpts.EnableMetrics {
			w.writerMetrics.workerDuration.WithLabelValues(app).Observe(float64(w.timer.Now() - eb.updateAt))
		}
		// 归还对象
		w.bufferPool.Put(eb)
	}
}

func (w *Writer) bufferIoWriteFlusher() {
	var (
		err    error
		ticker = time.NewTicker(time.Millisecond * 5000)
	)

	defer func() {
		ticker.Stop()
		w.wg.Done()
	}()

	for {
		select {
		case <-ticker.C:
			// 执行刷新缓冲
			w.bufferIoLock.Lock()
			if err = w.bufferIoWriter.Flush(); err != nil {
				log.Printf("data flush error, id: %s, error: %s", w.id, err.Error())
			}
			w.bufferIoLock.Unlock()
			break
		case <-w.stopCtx.Done(): // 如果 stopCtx 被取消，结束循环
			return
		}
	}
}

func (w *Writer) writeBytes(p []byte) (int, error) {
	if w.featureOpts.EnableBufferedIoWriter {
		w.bufferIoLock.Lock()
		defer w.bufferIoLock.Unlock()
		if len(p) > w.bufferIoWriter.Available() && w.bufferIoWriter.Buffered() > 0 {
			if err := w.bufferIoWriter.Flush(); err != nil {
				return 0, err
			}
		}
	}

	return w.writer.Write(p)
}

func validFeatureOpts(opts *FeatureOpts) *FeatureOpts {
	if opts != nil {
		return opts
	}

	return NewDefaultFeatureOpts()
}
