package client

import boost "gitee.com/ayun2001/ok-boost-go"

type requestLogger struct{}

func newRequestLogger() *requestLogger {
	return &requestLogger{}
}

func (r *requestLogger) Errorf(format string, v ...interface{}) {
	boost.Logger.Errorf(format, v)
}

func (r *requestLogger) Warnf(format string, v ...interface{}) {
	boost.Logger.Warnf(format, v)
}

func (r *requestLogger) Debugf(format string, v ...interface{}) {
	boost.Logger.Debugf(format, v)
}
