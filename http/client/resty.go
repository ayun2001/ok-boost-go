package client

import (
	"crypto/tls"
	"net/http"
	"sync"
	"time"

	boost "gitee.com/ayun2001/ok-boost-go"
	"gitee.com/ayun2001/ok-boost-go/pkg/codec/json"
	"gitee.com/ayun2001/ok-boost-go/pkg/httptool"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/bwmarrin/snowflake"
	"github.com/go-resty/resty/v2"
	"go.uber.org/zap"
	"golang.org/x/time/rate"
)

const (
	ConstHttpRequestIdleConnTimeout = 20 * 1000
	ConstInfinityQueryPerSecond     = 0
)

type HttpClientConfig struct {
	DisableCompression bool               `json:"disableCompression,omitempty" yaml:"disableCompression,omitempty"` // 关闭 gzip 解压
	DisableKeepalive   bool               `json:"disableKeepalive,omitempty" yaml:"disableKeepalive,omitempty"`     // 关闭 keepalive 长连接
	ConnTimeout        uint32             `json:"connectTimeout,omitempty" yaml:"connectTimeout,omitempty"`         // client 连接服务器超时时间
	IdleTimeout        uint32             `json:"idleTimeout,omitempty" yaml:"idleTimeout,omitempty"`               // client 会话空闲超时时间
	InsecureSkipVerify bool               `json:"insecureSkipVerify,omitempty" yaml:"insecureSkipVerify,omitempty"` // 忽略 https 证书校验
	QueryPerSecond     uint32             `json:"queryPerSecond,omitempty" yaml:"queryPerSecond,omitempty"`         // 每秒最大请求数
	Logger             *zap.SugaredLogger `json:"-" yaml:"-"`
}

type FeatureOpts struct {
	EnableMetrics           bool `json:"enableMetrics,omitempty" yaml:"enableMetrics,omitempty"` // 开启 metrics 记录
	EnableParseResponse     bool `json:"enableParseResponse,omitempty" yaml:"enableParseResponse,omitempty"`
	EnableRecordContextBody bool `json:"enableRecordContextBody,omitempty" yaml:"enableRecordContextBody,omitempty"`
}

type Pool struct {
	clientMetrics *clientMetrics
	clientConf    *HttpClientConfig
	featureOpts   *FeatureOpts
	closeOnce     sync.Once
	client        *resty.Client
	lock          sync.RWMutex
	idGenerator   *snowflake.Node
}

func NewDefaultFeatureOpts() *FeatureOpts {
	return &FeatureOpts{
		EnableMetrics:           true,
		EnableParseResponse:     true,
		EnableRecordContextBody: false,
	}
}

func NewDefaultHttpClientConfig() *HttpClientConfig {
	return &HttpClientConfig{
		DisableCompression: false,
		DisableKeepalive:   false,
		ConnTimeout:        ConstHttpRequestIdleConnTimeout,
		IdleTimeout:        ConstHttpRequestIdleConnTimeout,
		InsecureSkipVerify: true,
	}
}

func NewHttpClientPool(clientConf *HttpClientConfig, featureOpts *FeatureOpts) *Pool {
	clientConf = validHttpClientConfig(clientConf)
	featureOpts = validFeatureOpts(featureOpts)

	var p = Pool{
		clientMetrics: newHttpClientMetrics(),
		clientConf:    clientConf,
		featureOpts:   featureOpts,
		closeOnce:     sync.Once{},
		lock:          sync.RWMutex{},
	}

	p.idGenerator, _ = snowflake.NewNode(utils.CreateRand().Int63n(-1 ^ (-1 << snowflake.NodeBits)))

	if p.featureOpts.EnableMetrics {
		p.clientMetrics.registerMetrics()
	}

	p.lock.Lock()
	p.client = p.NewRestyClient()
	p.lock.Unlock()

	return &p
}

// NewClient 创建 resty 对象，连接不复用
func (p *Pool) NewRestyClient() *resty.Client {
	// 创建一个 resty.Client 对象
	r := resty.New()
	// 设置 logger，可在每个 request 被发送前打印出该 request 的详细信息
	r.SetLogger(newRequestLogger())
	// 设置 http 请求 limit, 限制每秒最大请求数, 0 表示不限制
	if p.clientConf.QueryPerSecond > 0 {
		r.SetRateLimiter(rate.NewLimiter(rate.Limit(p.clientConf.QueryPerSecond), 5))
	}
	// 配置 json 序列化和反序列化函数，使得 resty 支持 json 类型的 request
	r.JSONMarshal = json.Marshal
	r.JSONUnmarshal = json.Unmarshal
	// 设置请求超时时间
	r.SetTimeout(time.Millisecond * time.Duration(p.clientConf.ConnTimeout))
	// 配置 http transport，控制 http 客户端最多创建多少闲置的连接池，并设定空闲超时时间
	transport := &http.Transport{
		DisableCompression: p.clientConf.DisableCompression,                                  // 是否禁用 gzip 压缩
		IdleConnTimeout:    time.Millisecond * time.Duration(p.clientConf.IdleTimeout),       // 设置 idle 连接过期时间
		DisableKeepAlives:  p.clientConf.DisableKeepalive,                                    // 是否开启 keepalive
		TLSClientConfig:    &tls.Config{InsecureSkipVerify: p.clientConf.InsecureSkipVerify}, // 忽略 https 证书校验
	}
	// 将配置好的 transport 告知 resty 使用
	r.SetTransport(transport)
	// 配置重定向策略，设置允许客户端最大跳转次数
	r.SetRedirectPolicy(resty.FlexibleRedirectPolicy(15))
	// 允许 get 方法带 body
	r.SetAllowGetMethodPayload(true)
	// 设置不解析响应 body，需要手动通过 resp.Body() 或者 resp.RawBody() 解析。
	r.SetDoNotParseResponse(!p.featureOpts.EnableParseResponse)
	// 添加 RetryConditionFunc，当请求返回状态码为 http.StatusTooManyRequests 时进行重试
	r.AddRetryCondition(func(res *resty.Response, err error) bool {
		return res != nil && res.StatusCode() == http.StatusTooManyRequests
	})
	r.OnBeforeRequest(func(c *resty.Client, req *resty.Request) error {
		req.Time = time.Now() // 重置事件，会话复用的时候，会导致这个计数器事件不重置
		return nil            // if its success otherwise return error
	})
	// 添加 OnError，记录请求日志
	r.OnError(p.errorCallback)
	// 添加 OnAfterResponse，记录响应日志
	r.OnAfterResponse(p.responseCallback)

	return r
}

// SetRestyClient 设置 resty client
func (p *Pool) SetRestyClient(c *resty.Client) {
	p.lock.Lock()
	defer p.lock.Unlock()
	p.client = c
}

// GetRestyClient 获取 resty client
func (p *Pool) GetRestyClient() *resty.Client {
	p.lock.RLock()
	defer p.lock.RUnlock()
	return p.client
}

// GetRequest 复用 client， 支持 http keepalive，并更新获得 client 时间
// 设置了 EnableParseResponse 为 false 后，需要自己手动解码返回值 Response.Body() 的字节流内容。
func (p *Pool) GetRequest() *resty.Request {
	p.lock.RLock()
	defer p.lock.RUnlock()
	return p.client.NewRequest()
}

// PutRequest 归还会话，空壳函数
func (p *Pool) PutRequest(_ interface{}) {}

func (p *Pool) Stop() {
	p.closeOnce.Do(func() {
		if p.featureOpts.EnableMetrics {
			p.clientMetrics.unRegisterMetrics()
		}
	})
}

func (p *Pool) errorCallback(req *resty.Request, err error) {
	// 变量
	var (
		requestBody         string
		responseBody        string
		responseCode        int
		latency             time.Duration
		responseContentType string
	)

	host := req.RawRequest.URL.Host
	path := GenerateRequestPath(req)

	if p.featureOpts.EnableRecordContextBody && httptool.CanRecordContextBody(req.RawRequest.Header) {
		requestBody, _ = GenerateRequestBody(req)
	}

	v, ok := err.(*resty.ResponseError)
	if ok {
		// v.Response contains the last response from the server
		// v.Err contains the original error
		if p.featureOpts.EnableRecordContextBody &&
			httptool.CanRecordContextBody(v.Response.Header()) &&
			v.Response.Body() != nil {
			responseBody = utils.BytesToString(v.Response.Body())
		}
		responseCode = v.Response.StatusCode()
		responseContentType = v.Response.Header().Get(httptool.ConstHttpHeaderContentType)
		latency = v.Response.Time()
	}

	boost.Logger.Errorw(
		"failed to obtain server resources",
		"haveResponded", ok, "error", err,
		"serverHost", host,
		"serverEndpoint", req.RawRequest.RemoteAddr,
		"path", path,
		"method", req.Method,
		"success", isRequestSuccess(responseCode),
		"status", responseCode,
		"latency", latency.String(),
		"requestContentType", req.RawRequest.Header.Get(httptool.ConstHttpHeaderContentType),
		"requestQuery", req.RawRequest.URL.RawQuery,
		"requestBody", requestBody,
		"responseContentType", responseContentType,
		"responseBody", responseBody,
	)
}

func (p *Pool) responseCallback(_ *resty.Client, resp *resty.Response) error {
	// 变量
	host := resp.Request.RawRequest.URL.Host
	requestPath := resp.Request.RawRequest.URL.Path
	path := GenerateRequestPath(resp.Request)
	statusStr := utils.Int64ToString(int64(resp.StatusCode()))
	latency := resp.Time()
	app := utils.ApplicationName

	// 记录 metrics
	if p.featureOpts.EnableMetrics {
		r := float64(latency.Milliseconds())
		p.clientMetrics.requestCount.WithLabelValues(statusStr, resp.Request.Method, host, requestPath, app).Inc()
		p.clientMetrics.requestLatency.WithLabelValues(statusStr, resp.Request.Method, host, requestPath, app).Set(r)
		p.clientMetrics.requestLatencies.WithLabelValues(host, app).Observe(r)
	}

	// 记录日志
	var requestBody, responseBody string

	if p.featureOpts.EnableRecordContextBody {
		if httptool.CanRecordContextBody(resp.Request.RawRequest.Header) {
			requestBody, _ = GenerateRequestBody(resp.Request)
		}
		if resp.Body() != nil && httptool.CanRecordContextBody(resp.Header()) {
			responseBody = utils.BytesToString(resp.Body())
		}
	}

	p.clientConf.Logger.Infow(
		"resty http client log",
		"requestID", p.idGenerator.Generate().String(),
		"serverHost", host,
		"serverEndpoint", resp.Request.RawRequest.RemoteAddr,
		"path", path,
		"method", resp.Request.Method,
		"success", isRequestSuccess(resp.StatusCode()),
		"status", resp.StatusCode(),
		"latency", latency.String(),
		"requestContentType", resp.Request.RawRequest.Header.Get(httptool.ConstHttpHeaderContentType),
		"requestQuery", resp.Request.RawRequest.URL.RawQuery,
		"requestBody", requestBody,
		"responseContentType", resp.Header().Get(httptool.ConstHttpHeaderContentType),
		"responseBody", responseBody,
	)

	// 不用主动处理 http client 关闭，防止请求内存泄露, 除非调用 SetDoNotParseResponse(true) 要处理 http.client body 的close
	// https://pkg.go.dev/github.com/go-resty/resty/v2#Request.SetDoNotParseResponse
	// SetDoNotParseResponse method instructs `Resty` not to parse the response body automatically. Resty exposes the raw response body as `io.ReadCloser`.
	// Also do not forget to close the body, otherwise you might get into connection leaks, no connection reuse.
	// Note: Response middlewares are not applicable, if you use this option. Basically you have taken over the control of response parsing from `Resty`.
	//
	// RawBody method exposes the HTTP raw response body. Use this method in-conjunction with `SetDoNotParseResponse`
	// option otherwise you get an error as `read err: http: read on closed response body`.
	//
	//  // Do not forget to close the body, otherwise you might get into connection leaks, no connection reuse.
	//  // Basically you have taken over the control of response parsing from `Resty`.
	//  func (r *Response) RawBody() io.ReadCloser {
	//  	if r.RawResponse == nil {
	//  	return nil
	//  }
	//  	return r.RawResponse.Body
	//  }
	if !p.featureOpts.EnableParseResponse {
		if rawBody := resp.RawBody(); rawBody != nil {
			if err := rawBody.Close(); err != nil {
				boost.Logger.Errorw("failed to close raw body", "error", err)
			}
		}
	}

	// 返回
	return nil
}

func isRequestSuccess(code int) bool {
	return code > 199 && code < 300
}

func validHttpClientConfig(conf *HttpClientConfig) *HttpClientConfig {
	if conf != nil {
		if conf.ConnTimeout <= 0 {
			conf.ConnTimeout = ConstHttpRequestIdleConnTimeout
		}
		if conf.IdleTimeout <= 0 {
			conf.IdleTimeout = ConstHttpRequestIdleConnTimeout
		}
		if conf.Logger == nil {
			conf.Logger = boost.Logger
		}
	} else {
		conf = NewDefaultHttpClientConfig()
	}

	return conf
}

func validFeatureOpts(opts *FeatureOpts) *FeatureOpts {
	if opts != nil {
		return opts
	}

	return NewDefaultFeatureOpts()
}
