package client

import (
	"errors"
	"io"

	"gitee.com/ayun2001/ok-boost-go/pkg/codec/json"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/go-resty/resty/v2"
)

var bodyFailedMessage = "failed to get request body"

func GenerateRequestPath(r *resty.Request) string {
	if len(r.RawRequest.URL.RawQuery) > 0 {
		return r.RawRequest.URL.RequestURI()
	}
	return r.RawRequest.URL.Path
}

func GenerateRequestBody(r *resty.Request) (string, error) {
	var bodyStr string
	switch body := r.Body.(type) {
	case nil:
		return bodyFailedMessage, errors.New("Missing request body")
	case string:
		bodyStr = body
	case []byte:
		bodyStr = utils.BytesToString(body)
	case io.Reader:
		bodyBytes, err := io.ReadAll(body)
		if err != nil {
			return bodyFailedMessage, err
		}
		bodyStr = utils.BytesToString(bodyBytes)
	default:
		bodyBytes, err := json.Marshal(body)
		if err != nil {
			return bodyFailedMessage, err
		}
		bodyStr = utils.BytesToString(bodyBytes)
	}
	return bodyStr, nil
}
