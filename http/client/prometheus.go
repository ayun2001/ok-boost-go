package client

import (
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/prometheus/client_golang/prometheus"
)

type clientMetrics struct {
	requestCount     *prometheus.CounterVec
	requestLatencies *prometheus.HistogramVec
	requestLatency   *prometheus.GaugeVec
}

func newHttpClientMetrics() *clientMetrics {
	return &clientMetrics{}
}

func (m *clientMetrics) registerMetrics() {
	var metricLabels = []string{"status", "method", "host", "path", "app"}
	var id = utils.GetRandIdString()

	m.requestCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "http_client_request_count",
			Help:        "The total number of HTTP requests made.",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	m.requestLatency = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "http_client_request_latency_seconds",
			Help:        "HTTP request latencies in seconds.",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	m.requestLatencies = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "http_client_request_latency",
			Help:        "HTTP request latencies in seconds.",
			ConstLabels: map[string]string{"id": id},
		}, []string{"host", "app"},
	)

	prometheus.MustRegister(m.requestCount, m.requestLatencies, m.requestLatency)
}

func (m *clientMetrics) unRegisterMetrics() {
	prometheus.Unregister(m.requestCount)
	prometheus.Unregister(m.requestLatency)
	prometheus.Unregister(m.requestLatencies)
}
