package server

import (
	"net/http/pprof"

	"gitee.com/ayun2001/ok-boost-go/http/server/common"
	"gitee.com/ayun2001/ok-boost-go/pkg/httptool"
	"github.com/gin-gonic/gin"
)

func httpProfHandler(g *gin.RouterGroup) {
	rg := g.Group(common.ConstHttpPprofUrlPath)

	// Get
	rg.GET("/", httptool.HttpHandlerFuncToGinHandlerFunc(pprof.Index))
	rg.GET("/cmdline", httptool.HttpHandlerFuncToGinHandlerFunc(pprof.Cmdline))
	rg.GET("/profile", httptool.HttpHandlerFuncToGinHandlerFunc(pprof.Profile))
	rg.GET("/symbol", httptool.HttpHandlerFuncToGinHandlerFunc(pprof.Symbol))
	rg.GET("/trace", httptool.HttpHandlerFuncToGinHandlerFunc(pprof.Trace))
	rg.GET("/allocs", httptool.HttpHandlerFuncToGinHandlerFunc(pprof.Handler("allocs").ServeHTTP))
	rg.GET("/block", httptool.HttpHandlerFuncToGinHandlerFunc(pprof.Handler("block").ServeHTTP))
	rg.GET("/goroutine", httptool.HttpHandlerFuncToGinHandlerFunc(pprof.Handler("goroutine").ServeHTTP))
	rg.GET("/heap", httptool.HttpHandlerFuncToGinHandlerFunc(pprof.Handler("heap").ServeHTTP))
	rg.GET("/mutex", httptool.HttpHandlerFuncToGinHandlerFunc(pprof.Handler("mutex").ServeHTTP))
	rg.GET("/threadcreate", httptool.HttpHandlerFuncToGinHandlerFunc(pprof.Handler("threadcreate").ServeHTTP))

	// Post
	rg.POST("/pprof/symbol", httptool.HttpHandlerFuncToGinHandlerFunc(pprof.Symbol))
}
