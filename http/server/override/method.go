package override

import (
	"sync"

	boost "gitee.com/ayun2001/ok-boost-go"
	"gitee.com/ayun2001/ok-boost-go/http/server/common"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

type MethodOverrideElement struct {
	OldMethod string `json:"oldMethod,omitempty" yaml:"oldMethod,omitempty"`
	NewMethod string `json:"newMethod,omitempty" yaml:"newMethod,omitempty"`
}

type MethodOverrideMapping struct {
	Elements map[string]MethodOverrideElement `json:"elements,omitempty" yaml:"elements,omitempty"`
	lock     sync.RWMutex                     `json:"-" yaml:"-"`
}

func (m *MethodOverrideMapping) Set(key string, value MethodOverrideElement) {
	m.lock.Lock()
	defer m.lock.Unlock()

	if m.Elements == nil {
		m.Elements = make(map[string]MethodOverrideElement)
	}

	m.Elements[key] = value
}

func (m *MethodOverrideMapping) Get(key string) (MethodOverrideElement, bool) {
	m.lock.RLock()
	defer m.lock.RUnlock()

	if m.Elements == nil {
		return MethodOverrideElement{}, false
	}

	element, ok := m.Elements[key]
	return element, ok
}

func (h *MethodOverrideMapping) GetOrCreate(key string, value MethodOverrideElement) MethodOverrideElement {
	h.lock.RLock()
	defer h.lock.RUnlock()

	if h.Elements == nil {
		return MethodOverrideElement{}
	}

	if element, ok := h.Elements[key]; ok {
		return element
	} else {
		h.Elements[key] = value
		return value
	}
}

func (m *MethodOverrideMapping) Delete(key string) {
	m.lock.Lock()
	defer m.lock.Unlock()

	if m.Elements == nil {
		return
	}

	delete(m.Elements, key)
}

type MethodOverrideConfig struct {
	Mapping *MethodOverrideMapping `json:"mapping,omitempty" yaml:"mapping,omitempty"` // 方法重写映射
	Logger  *zap.SugaredLogger     `json:"-" yaml:"-"`
}

type MethodOverride struct {
	overrideConf    *MethodOverrideConfig
	overrideMetrics *methodOverrideMetrics
	featureOpts     *FeatureOpts
	closeOnce       sync.Once
}

type FeatureOpts struct {
	EnableMetrics bool `json:"enableMetrics,omitempty" yaml:"enableMetrics,omitempty"` // 开启 metrics 记录
}

func NewDefaultFeatureOpts() *FeatureOpts {
	return &FeatureOpts{
		EnableMetrics: true,
	}
}

func NewDefaultMethodOverrideConfig() *MethodOverrideConfig {
	return &MethodOverrideConfig{
		Mapping: &MethodOverrideMapping{
			Elements: map[string]MethodOverrideElement{},
			lock:     sync.RWMutex{},
		},
	}
}

func NewMethodOverride(overrideConf *MethodOverrideConfig, featureOpts *FeatureOpts) *MethodOverride {
	featureOpts = validFeatureOpts(featureOpts)
	overrideConf = validMethodOverrideConfig(overrideConf)

	mo := MethodOverride{
		overrideConf:    overrideConf,
		overrideMetrics: newMethodOverrideMetrics(),
		featureOpts:     NewDefaultFeatureOpts(),
		closeOnce:       sync.Once{},
	}

	if featureOpts.EnableMetrics {
		mo.overrideMetrics.registerMetrics()
	}

	return &mo
}

func (m *MethodOverride) Stop() {
	m.closeOnce.Do(func() {
		if m.featureOpts.EnableMetrics {
			m.overrideMetrics.unRegisterMetrics()
		}
	})
}

func (m *MethodOverride) OverrideHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		// 如果请求资源需要跳过则继续后面的处理程序
		if common.SkipResources(c) {
			// 继续执行后面的处理程序
			c.Next()
			// 结束本次中间件的处理
			return
		}

		// 记录数据
		// 获取请求的路由，方法和应用名称
		path := c.Request.URL.Path
		method := c.Request.Method
		app := utils.ApplicationName
		reqMetadata := common.GetRequestMetadata(c)

		// 如果请求的方法不是 GET，POST，DELETE，UPDATE，PUT, PATCH 则直接返回
		if method != "GET" && method != "POST" && method != "DELETE" && method != "UPDATE" && method != "PUT" && method != "PATCH" {
			// 继续执行后面的处理程序
			c.Next()
			// 结束本次中间件的处理
			return
		}

		// 设置日志模块
		common.CompareAndSwapRequestLogger(c, m.overrideConf.Logger)

		// 如果请求的方法是 GET，POST，DELETE，UPDATE，PUT, PATCH 则判断是否需要重写
		newMethod := c.Request.Header.Get(common.ConstHttpMethodOverride)
		if newMethod != "" {
			c.Request.Method = newMethod
			// 如果启用了计量选项，则使用 methodOverrideMetrics 对象记录请求的统计指标
			if m.featureOpts.EnableMetrics {
				m.overrideMetrics.overrideCount.WithLabelValues(method, newMethod, path, app, reqMetadata.Listener).Inc()
			}
			// 记录日志
			m.overrideConf.Logger.Infow("request method override", "oldMethod", method, "newMethod", newMethod, "path", path, "clientIP", reqMetadata.ClientIP, "app", app, "listener", reqMetadata.Listener)
		} else {
			if element, ok := m.overrideConf.Mapping.Get(path); ok {
				if element.OldMethod == method {
					// 如果启用了计量选项，则使用 methodOverrideMetrics 对象记录请求的统计指标
					c.Request.Method = element.NewMethod
					if m.featureOpts.EnableMetrics {
						m.overrideMetrics.overrideCount.WithLabelValues(method, newMethod, path, app, reqMetadata.Listener).Inc()
					}
					// 记录日志
					m.overrideConf.Logger.Infow("request method override", "oldMethod", method, "newMethod", newMethod, "path", path, "clientIP", reqMetadata.ClientIP, "app", app, "listener", reqMetadata.Listener)
				}
			}
		}

		// 调用下一个中间件处理程序
		c.Next()
	}
}

func validMethodOverrideConfig(conf *MethodOverrideConfig) *MethodOverrideConfig {
	if conf != nil {
		if conf.Logger == nil {
			conf.Logger = boost.Logger
		}
		if conf.Mapping == nil {
			conf.Mapping = &MethodOverrideMapping{}
		}
	} else {
		conf = NewDefaultMethodOverrideConfig()
	}

	return conf
}

func validFeatureOpts(opts *FeatureOpts) *FeatureOpts {
	if opts != nil {
		return opts
	}

	return NewDefaultFeatureOpts()
}
