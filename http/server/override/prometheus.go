package override

import (
	"gitee.com/ayun2001/ok-boost-go/http/server/common"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/prometheus/client_golang/prometheus"
)

type methodOverrideMetrics struct {
	overrideCount *prometheus.CounterVec
}

func newMethodOverrideMetrics() *methodOverrideMetrics {
	return &methodOverrideMetrics{}
}

func (m *methodOverrideMetrics) registerMetrics() {
	var metricLabels = []string{"method", "newMethod", "path", "app", common.ConstPromServiceEndpoint}
	var id = utils.GetRandIdString()

	m.overrideCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "http_server_request_method_override_count",
			Help:        "The total number of HTTP request methods overrided.",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	prometheus.MustRegister(m.overrideCount)
}

func (m *methodOverrideMetrics) unRegisterMetrics() {
	prometheus.Unregister(m.overrideCount)
}

type hostOverrideMetrics struct {
	overrideCount *prometheus.CounterVec
}

func newHostOverrideMetrics() *hostOverrideMetrics {
	return &hostOverrideMetrics{}
}

func (h *hostOverrideMetrics) registerMetrics() {
	var metricLabels = []string{"method", "path", "host", "newHost", "app", common.ConstPromServiceEndpoint}
	var id = utils.GetRandIdString()

	h.overrideCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "http_server_request_host_override_count",
			Help:        "The total number of HTTP request hosts overrided.",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	prometheus.MustRegister(h.overrideCount)
}

func (h *hostOverrideMetrics) unRegisterMetrics() {
	prometheus.Unregister(h.overrideCount)
}
