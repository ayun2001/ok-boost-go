package override

import (
	"net/http"
	"sync"

	boost "gitee.com/ayun2001/ok-boost-go"
	"gitee.com/ayun2001/ok-boost-go/http/server/common"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

type HostOverrideConfig struct {
	HostOverrideFunc func(header *http.Request) (string, error) `json:"-" yaml:"-"`
	Logger           *zap.SugaredLogger                         `json:"-" yaml:"-"`
}

type HostOverride struct {
	overrideConf    *HostOverrideConfig
	overrideMetrics *hostOverrideMetrics
	featureOpts     *FeatureOpts
	closeOnce       sync.Once
}

func NewDefaultHostOverrideConfig() *HostOverrideConfig {
	return &HostOverrideConfig{}
}

func NewHostOverride(overrideConf *HostOverrideConfig, featureOpts *FeatureOpts) *HostOverride {
	featureOpts = validFeatureOpts(featureOpts)
	overrideConf = validHostOverrideConfig(overrideConf)

	ho := HostOverride{
		overrideConf:    overrideConf,
		overrideMetrics: newHostOverrideMetrics(),
		featureOpts:     NewDefaultFeatureOpts(),
		closeOnce:       sync.Once{},
	}

	if featureOpts.EnableMetrics {
		ho.overrideMetrics.registerMetrics()
	}

	return &ho
}

func (h *HostOverride) Stop() {
	h.closeOnce.Do(func() {
		if h.featureOpts.EnableMetrics {
			h.overrideMetrics.unRegisterMetrics()
		}
	})
}

func (h *HostOverride) OverrideHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		// 如果请求资源需要跳过则继续后面的处理程序
		if common.SkipResources(c) {
			// 继续执行后面的处理程序
			c.Next()
			// 结束本次中间件的处理
			return
		}

		// 记录数据
		// 获取请求的路由，方法和应用名称
		path := c.Request.URL.Path
		method := c.Request.Method
		app := utils.ApplicationName
		reqMetadata := common.GetRequestMetadata(c)

		// 如果请求的方法不是 GET，POST，DELETE，UPDATE，PUT, PATCH 则直接返回
		if method != "GET" && method != "POST" && method != "DELETE" && method != "UPDATE" && method != "PUT" && method != "PATCH" {
			// 继续执行后面的处理程序
			c.Next()
			// 结束本次中间件的处理
			return
		}

		// 设置日志模块
		common.CompareAndSwapRequestLogger(c, h.overrideConf.Logger)

		newHost := c.Request.Header.Get(common.ConstHttpHostOverride)
		if newHost != "" {
			c.Request.Host = newHost
			// 如果启用了计量选项，则使用 hostOverrideMetrics 对象记录请求的统计指标
			if h.featureOpts.EnableMetrics {
				h.overrideMetrics.overrideCount.WithLabelValues(method, path, reqMetadata.Host, newHost, app, reqMetadata.Listener).Inc()
			}
			// 记录日志
			h.overrideConf.Logger.Infow("request host override", "oldHost", reqMetadata.Host, "newHost", newHost, "path", path, "clientIP", reqMetadata.ClientIP, "app", app, "listener", reqMetadata.Listener)
		} else {
			var err error
			if newHost, err = h.overrideConf.HostOverrideFunc(c.Request); err != nil && newHost != "" {
				c.Request.Host = newHost
				// 如果启用了计量选项，则使用 hostOverrideMetrics 对象记录请求的统计指标
				if h.featureOpts.EnableMetrics {
					h.overrideMetrics.overrideCount.WithLabelValues(method, path, reqMetadata.Host, newHost, app, reqMetadata.Listener).Inc()
				}
				// 记录日志
				h.overrideConf.Logger.Infow("request host override", "oldHost", reqMetadata.Host, "newHost", newHost, "path", path, "clientIP", reqMetadata.ClientIP, "app", app, "listener", reqMetadata.Listener)
			}
		}

		// 调用下一个中间件处理程序
		c.Next()
	}
}

func validHostOverrideConfig(conf *HostOverrideConfig) *HostOverrideConfig {
	if conf != nil {
		if conf.Logger == nil {
			conf.Logger = boost.Logger
		}
	} else {
		conf = NewDefaultHostOverrideConfig()
	}

	return conf
}
