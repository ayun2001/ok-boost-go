package server

import (
	boost "gitee.com/ayun2001/ok-boost-go"
	"github.com/gin-gonic/gin"
)

type HttpService struct {
	routeHandler func(*gin.RouterGroup)
}

func (s *HttpService) RoutesRegistry(e *Engine) {
	s.routeHandler(e.rootRouteGroup)
}

func NewHttpService(f func(*gin.RouterGroup)) *HttpService {
	if f != nil {
		return &HttpService{routeHandler: f}
	} else {
		boost.Logger.Fatalw("router group handler func is null")
		return nil
	}
}
