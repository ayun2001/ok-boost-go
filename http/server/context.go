package server

import (
	"bytes"
	"errors"
	"io"

	boost "gitee.com/ayun2001/ok-boost-go"
	"gitee.com/ayun2001/ok-boost-go/http/server/common"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/gin-gonic/gin"
)

func newRequestMetadata() *common.RequestMetadata {
	return &common.RequestMetadata{}
}

var DetermineRequestContentTypeError = errors.New("failed to determine the request content type. set http header with Content-Type")

func GenerateRequestPath(c *gin.Context) string {
	if len(c.Request.URL.RawQuery) > 0 {
		return c.Request.URL.RequestURI()
	}
	return c.Request.URL.Path
}

// GenerateRequestBody 从Gin的上下文中读取HTTP请求的Body，并将其存储到一个Buffer Pool对象中。
func GenerateRequestBody(c *gin.Context) string {
	// 检查是否已经有相关Buffer Pool对象，如果没有，则创建一个新的实例
	var b *common.ContextBodyBuff
	if o, ok := c.Get(common.ConstRequestBodyBufferKey); ok {
		b = o.(*common.ContextBodyBuff)
	} else {
		b = common.NewContextBodyBuff()
	}

	// 读取HTTP请求的Body
	// c.Request.Body = io.NopCloser(b.GetBuffer()) 这里 c.Request.Body 就是一个 io.ReadCloser
	// io.ReadAll 会调用 Read 接口，将内部的数据全部抽走，此时 b.GetBuffer() 返回的 bytes.Buffer 对象就是一个空对象。
	body, err := io.ReadAll(c.Request.Body)
	if err != nil {
		// 如果在读取Body时出现错误，则记录日志
		body = utils.StringToBytes("failed to get request body")
		boost.Logger.Errorw(utils.BytesToString(body), "error", err)
	}

	// 把内容写入 Buffer Pool 对象中
	_, err = b.GetBuffer().Write(body)
	if err != nil {
		// 如果在把内容写入 Buff Pool 是出现了错误，则存储原始的内容
		c.Request.Body = io.NopCloser(bytes.NewBuffer(body))
		boost.Logger.Warnw("failed to write the request body with buffer pool, created a new object", "error", err)
	} else {
		c.Request.Body = io.NopCloser(b.GetBuffer())
	}

	return utils.BytesToString(body)
}

func GenerateResponseBody(c *gin.Context) string {
	if o, ok := c.Get(common.ConstResponseBodyBufferKey); ok {
		return utils.BytesToString(o.(*common.ContextBodyBuff).GetBuffer().Bytes())
	} else {
		return "failed to get response body"
	}
}

// ParseRequestBody 将 request body 解析为指定类型 value 的变量，emptyRequestBodyContent 表示是否允许为空。
func ParseRequestBody(c *gin.Context, value interface{}, emptyRequestBodyContent bool) error {
	// 判断 ContentType 是否为空
	if c.ContentType() == "" {
		return DetermineRequestContentTypeError
	}

	err := c.ShouldBind(value)
	if err != nil {
		if emptyRequestBodyContent && len(GenerateRequestBody(c)) <= 0 {
			return nil
		}
	}

	return err
}
