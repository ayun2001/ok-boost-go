package common

const (
	ConstPromMetricUrlPath          = "/metrics"
	ConstHttpHealthCheckUrlPath     = "/ping"
	ConstRootUrlPath                = "/"
	ConstHttpSwaggerUrlPath         = "/docs"
	ConstHttpPprofUrlPath           = "/debug/pprof"
	ConstHttpJwtCreateTokenUrlPath  = "/jwt/create"
	ConstHttpJwtRefreshTokenUrlPath = "/jwt/refresh"
)

const (
	ConstRequestBodyBufferKey  = "REQUEST_BOD_YzdiT5HaFaMF7ZfO556rZRYqn"
	ConstRequestIdGeneratorKey = "REQUEST_ID_HbU3Z3opcTKBSe2O5yZQnSGD"
	ConstRequestMetadataKey    = "REQUEST_METADATA_J4PMFhL1ruM0PT64vRMCwhBu"
	ConstResponseBodyBufferKey = "RESPONSE_BODY_DT6IKLsNULVD3bTgnz1QJbeN"
	ConstRequestLoggerKey      = "REQUEST_LOGGER_3Z3opcTKBSe2O5yZQnSGD"
)

const (
	ConstHttpRequestIdleConnTimeout = 20 * 1000
)

const (
	ConstPromServiceEndpoint = "listener"
	ConstHttpMethodOverride  = "X-HTTP-Method-Override"
	ConstHttpHostOverride    = "X-HTTP-Host-Override"
	ConstHttpRequestID       = "X-Request-Id"
)
