package common

import (
	"strings"

	"github.com/gin-gonic/gin"
)

func SkipResources(c *gin.Context) bool {
	if c.Request.URL.Path == ConstPromMetricUrlPath ||
		c.Request.URL.Path == ConstHttpHealthCheckUrlPath ||
		strings.HasPrefix(c.Request.URL.Path, ConstHttpSwaggerUrlPath) ||
		strings.HasPrefix(c.Request.URL.Path, ConstHttpPprofUrlPath) ||
		strings.HasPrefix(c.Request.URL.Path, ConstHttpJwtCreateTokenUrlPath) {
		return true
	}
	return false
}
