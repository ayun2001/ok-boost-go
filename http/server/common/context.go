package common

import (
	"bytes"
	"sync/atomic"
	"unsafe"

	boost "gitee.com/ayun2001/ok-boost-go"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

const defaultBufferSize = 2048

type ContextBodyBuff struct {
	buff *bytes.Buffer
}

func (b *ContextBodyBuff) GetBuffer() *bytes.Buffer {
	return b.buff
}

func NewContextBodyBuff() *ContextBodyBuff {
	return &ContextBodyBuff{
		buff: bytes.NewBuffer(make([]byte, 0, defaultBufferSize)),
	}
}

type RequestMetadata struct {
	Host         string
	ClientIP     string
	Listener     string
	OriginWriter gin.ResponseWriter
}

type ResponseBodyWriter struct {
	gin.ResponseWriter
	bodyBuf *bytes.Buffer
}

func NewResponseBodyWriter(w gin.ResponseWriter, b *bytes.Buffer) *ResponseBodyWriter {
	return &ResponseBodyWriter{
		ResponseWriter: w,
		bodyBuf:        b,
	}
}

func (w *ResponseBodyWriter) Write(b []byte) (int, error) {
	if count, err := w.bodyBuf.Write(b); err != nil {
		return count, err
	}
	return w.ResponseWriter.Write(b)
}

func (w *ResponseBodyWriter) WriteString(s string) (int, error) {
	if count, err := w.bodyBuf.WriteString(s); err != nil {
		return count, err
	}
	return w.ResponseWriter.WriteString(s)
}

func (w *ResponseBodyWriter) Reset() {
	w.bodyBuf = nil
}

func GetRequestMetadata(c *gin.Context) (metadata *RequestMetadata) {
	// 首先尝试从 gin.Context 中获取 metadata 的指针
	if rawData, exists := c.Get(ConstRequestMetadataKey); exists && rawData != nil {
		// 如果存在，断言数据类型，并返回指针
		if typedData, ok := rawData.(*RequestMetadata); ok && typedData != nil {
			metadata = typedData
			return
		}
	}

	// 否则手动创建 RequestMetadata 并设置其值
	metadata = new(RequestMetadata)
	metadata.ClientIP = c.ClientIP()
	metadata.Host = c.Request.Host
	metadata.Listener = "unknown"
	metadata.OriginWriter = c.Writer

	return
}

// 此处日志受外部定义的日志对象影响 ginAccessLogger
// 如果这里是异步的日志，可能存在丢日志的风险，性能很高
// 如果是同步日志，正常写入日志，但是性能会受影响
func GetRequestLogger(c *gin.Context) *zap.SugaredLogger {
	if rawData, exists := c.Get(ConstRequestLoggerKey); exists && rawData != nil {
		if typedData, ok := rawData.(*zap.SugaredLogger); ok && typedData != nil {
			return typedData
		}
	}
	return boost.Logger
}

// 将 Http Request Context 中 Logger 通过 atomic 设置到对应的对象指针中
func CompareAndSwapRequestLogger(c *gin.Context, logger *zap.SugaredLogger) {
	atomic.CompareAndSwapPointer((*unsafe.Pointer)(unsafe.Pointer(&logger)), nil, unsafe.Pointer(GetRequestLogger(c)))
}
