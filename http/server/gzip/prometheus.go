package gzip

import (
	"gitee.com/ayun2001/ok-boost-go/http/server/common"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/prometheus/client_golang/prometheus"
)

type compressMetrics struct {
	requestCompress   *prometheus.HistogramVec
	requestRatioCount *prometheus.CounterVec
	requestRatio      *prometheus.GaugeVec
}

func newCompressMetrics() *compressMetrics {
	return &compressMetrics{}
}

func (m *compressMetrics) registerMetrics() {
	var metricLabels = []string{"method", "path", "app", common.ConstPromServiceEndpoint}
	var id = utils.GetRandIdString()

	m.requestRatioCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "http_server_request_compressed_count",
			Help:        "The total number of HTTP requests compressed.",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	m.requestRatio = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "http_server_request_compress_ratio",
			Help:        "HTTP request compress ratio.",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	m.requestCompress = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "http_server_request_compress",
			Help:        "HTTP request compress ratio distribution.",
			ConstLabels: map[string]string{"id": id},
		}, []string{"app", common.ConstPromServiceEndpoint},
	)

	prometheus.MustRegister(m.requestRatioCount, m.requestRatio, m.requestCompress)
}

func (m *compressMetrics) unRegisterMetrics() {
	prometheus.Unregister(m.requestRatio)
	prometheus.Unregister(m.requestCompress)
	prometheus.Unregister(m.requestRatioCount)
}
