package gzip

import "sync"

type stringHashSet struct {
	lock  sync.RWMutex
	store map[string]struct{}
}

var itemExists = struct{}{}

func newStringHashSet(ss []string) *stringHashSet {
	hashSet := stringHashSet{
		lock:  sync.RWMutex{},
		store: make(map[string]struct{}),
	}

	hashSet.lock.Lock()
	defer hashSet.lock.Unlock()

	if len(ss) > 0 {
		for _, s := range ss {
			hashSet.store[s] = itemExists
		}
	}

	return &hashSet

}

func (s *stringHashSet) Contain(i string) bool {
	s.lock.RLock()
	defer s.lock.RUnlock()
	_, ok := s.store[i]
	return ok
}

func (s *stringHashSet) Add(i string) {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.store[i] = itemExists
}

func (s *stringHashSet) Delete(i string) {
	s.lock.Lock()
	defer s.lock.Unlock()
	delete(s.store, i)
}

func (s *stringHashSet) Len() int {
	s.lock.RLock()
	defer s.lock.RUnlock()
	return len(s.store)
}

func (s *stringHashSet) Clear() {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.store = make(map[string]struct{})
}
