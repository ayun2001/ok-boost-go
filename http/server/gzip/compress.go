package gzip

import (
	"compress/gzip"
	"io"
	"net/http"
	"path/filepath"
	"strconv"
	"strings"
	"sync"

	boost "gitee.com/ayun2001/ok-boost-go"
	"gitee.com/ayun2001/ok-boost-go/http/server/common"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

const TREEIFY_THRESHOLD = 1 << 8

type Compress struct {
	compressConf    *CompressConfig
	compressMetrics *compressMetrics
	writerPool      sync.Pool
	featureOpts     *FeatureOpts
	closeOnce       sync.Once
}

type FeatureOpts struct {
	EnableMetrics bool `json:"enableMetrics,omitempty" yaml:"enableMetrics,omitempty"` // 开启 metrics 记录
}

type CompressConfig struct {
	Level                 uint8              `json:"level,omitempty" yaml:"level,omitempty"`
	ExcludedPaths         []string           `json:"excludedPaths,omitempty" yaml:"excludedPaths,omitempty"`
	ExcludedExtensions    []string           `json:"excludedExtensions,omitempty" yaml:"excludedExtensions,omitempty"`
	excludedPathsSet      *stringHashSet     `json:"-" yaml:"-"`
	excludedExtensionsSet *stringHashSet     `json:"-" yaml:"-"`
	Logger                *zap.SugaredLogger `json:"-" yaml:"-"`
}

func (c *CompressConfig) InitializeMap() {
	c.excludedPathsSet = newStringHashSet(c.ExcludedPaths)
	c.excludedExtensionsSet = newStringHashSet(c.ExcludedExtensions)
}

func NewDefaultFeatureOpts() *FeatureOpts {
	return &FeatureOpts{
		EnableMetrics: true,
	}
}

func NewDefaultCompressConfig() *CompressConfig {
	return &CompressConfig{
		Level:              ConstDefaultCompression,
		ExcludedPaths:      []string{},
		ExcludedExtensions: []string{},
	}
}

func NewCompress(compressConf *CompressConfig, featureOpts *FeatureOpts) *Compress {
	compressConf = validCompressConfig(compressConf)
	featureOpts = validFeatureOpts(featureOpts)

	cs := Compress{
		compressConf:    compressConf,
		compressMetrics: newCompressMetrics(),
		featureOpts:     featureOpts,
		closeOnce:       sync.Once{},
		writerPool: sync.Pool{
			New: func() interface{} {
				gz, err := gzip.NewWriterLevel(io.Discard, int(ConstDefaultCompression))
				if err != nil {
					boost.Logger.Errorf("gzip.NewWriterLevel error: %v", err)
					return nil
				}
				return gz
			},
		},
	}

	if featureOpts.EnableMetrics {
		cs.compressMetrics.registerMetrics()
	}

	return &cs
}

func (cs *Compress) Stop() {
	cs.closeOnce.Do(func() {
		if cs.featureOpts.EnableMetrics {
			cs.compressMetrics.unRegisterMetrics()
		}
	})
}

// CompressHandler 为压缩中间件，返回 gin.HandlerFunc 类型的函数处理路由请求
func (cs *Compress) CompressHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		// 如果请求资源需要跳过则继续后面的处理程序
		if common.SkipResources(c) || !cs.shouldCompress(c.Request) {
			// 继续执行后面的处理程序
			c.Next()
			// 结束本次中间件的处理
			return
		}

		path := c.Request.URL.Path
		method := c.Request.Method
		app := utils.ApplicationName
		reqMetadata := common.GetRequestMetadata(c)
		contextWriter := c.Writer

		// 设置响应头部信息，表示响应数据经过 gzip 压缩
		c.Header("Content-Encoding", "gzip")
		c.Header("Vary", "Accept-Encoding")

		// 创建 GzipWriter
		gz := cs.writerPool.Get().(*gzip.Writer)
		if gz == nil {
			// 继续执行后面的处理程序
			c.Next()
			// 结束本次中间件的处理
			return
		}
		gz.Reset(reqMetadata.OriginWriter) // 重置 GzipWriter, 这里一定要用最原始 gin.ResponseWriter

		// 关闭 GzipWriter
		defer func() {
			gz.Close()
			gz.Reset(io.Discard)
			cs.writerPool.Put(gz)
		}()

		// 从上下文中获取缓冲区
		o, ok := c.Get(common.ConstResponseBodyBufferKey)
		if !ok {
			// 继续执行后面的处理程序
			c.Next()
			// 结束本次中间件的处理
			return
		}

		// 设置 gin.Context.Writer 为 GzipWriter，这里一定要用最原始 gin.ResponseWriter
		buff := o.(*common.ContextBodyBuff).GetBuffer()
		c.Writer = NewWriter(reqMetadata.OriginWriter, buff, gz)

		// 继续执行链中的处理函数
		c.Next()

		// 设置响应头部信息，表示响应数据经过 gzip 压缩
		compressBodySize := c.Writer.Size()
		c.Header("Content-Length", strconv.Itoa(compressBodySize))

		// 重置 gin.Context.Writer 为原始的 gin.ResponseWriter
		c.Writer = contextWriter

		// 设置 metrics
		if cs.featureOpts.EnableMetrics {
			buffLength := buff.Len()
			if buffLength > 0 {
				r := float64(compressBodySize) / float64(buffLength)
				cs.compressMetrics.requestCompress.WithLabelValues(app, reqMetadata.Listener).Observe(r)
				cs.compressMetrics.requestRatio.WithLabelValues(method, path, app, reqMetadata.Listener).Set(r)
				cs.compressMetrics.requestRatioCount.WithLabelValues(method, path, app, reqMetadata.Listener).Inc()
			}
		}
	}
}

// shouldCompress 判断一个 HTTP 请求是否需要进行 gzip 压缩
// 根据请求头中的 Accept-Encoding、Connection 和 Accept 字段以及 URL 路径和扩展名，
// 来判断是否需要对请求进行压缩。如果需要压缩则返回 true，否则返回 false。
func (cs *Compress) shouldCompress(req *http.Request) bool {
	// 如果请求头中不包含 "gzip" 编码，或者 Connection 头字段中包含 "Upgrade"，
	// 或者 Accept 头字段中包含 "text/event-stream"，则说明不需要压缩
	if !strings.Contains(req.Header.Get("Accept-Encoding"), "gzip") ||
		strings.Contains(req.Header.Get("Connection"), "Upgrade") ||
		strings.Contains(req.Header.Get("Accept"), "text/event-stream") {
		return false
	}

	// 如果 URL 路径或者扩展名在压缩配置中指定的排除列表中，则说明不需要压缩
	if cs.inStringSlice(req.URL.Path, cs.compressConf.ExcludedPaths) ||
		cs.inStringSlice(filepath.Ext(req.URL.Path), cs.compressConf.ExcludedExtensions) {
		return false
	}

	// 如果以上两个条件都不满足，则说明需要对该请求进行压缩
	return true
}

// inStringSliceOrTree 判断一个字符串是否在给定的字符串切片中出现过
// 如果切片长度小于 TREEIFY_THRESHOLD ，则使用遍历判断方式，
// 如果大于等于 TREEIFY_THRESHOLD，则将切片转换成哈希集合，再用哈希集合进行判断
func (cs *Compress) inStringSlice(i string, s []string) bool {
	l := len(i)

	// 如果没有数据
	if l <= 0 {
		return false
	}

	// 数据长度判断
	if l < TREEIFY_THRESHOLD {
		for _, v := range s {
			if i == v {
				return true
			}
		}
		return false
	} else {
		// 判断哈希集合中是否包含输入的字符串
		return cs.compressConf.excludedPathsSet.Contain(i) || cs.compressConf.excludedExtensionsSet.Contain(i)
	}
}

func validCompressConfig(conf *CompressConfig) *CompressConfig {
	if conf != nil {
		if conf.Level > gzip.BestCompression {
			conf.Level = ConstDefaultCompression
		}
		if conf.Logger == nil {
			conf.Logger = boost.Logger
		}
	} else {
		conf = NewDefaultCompressConfig()
	}

	if conf.excludedPathsSet == nil {
		conf.excludedPathsSet = newStringHashSet(conf.ExcludedPaths)
	}

	if conf.excludedExtensionsSet == nil {
		conf.excludedExtensionsSet = newStringHashSet(conf.ExcludedExtensions)
	}

	return conf
}

func validFeatureOpts(opts *FeatureOpts) *FeatureOpts {
	if opts != nil {
		return opts
	}

	return NewDefaultFeatureOpts()
}
