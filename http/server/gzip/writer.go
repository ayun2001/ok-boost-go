package gzip

import (
	"bytes"
	"compress/gzip"
	"io"

	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/gin-gonic/gin"
)

const (
	ConstDefaultCompression = uint8(6)
	ConstNoCompression      = uint8(gzip.NoCompression)
)

type Writer struct {
	gin.ResponseWriter
	bodyBuf  *bytes.Buffer
	gzWriter io.Writer
}

func NewWriter(ginW gin.ResponseWriter, buff *bytes.Buffer, gzipW *gzip.Writer) *Writer {
	return &Writer{
		ResponseWriter: ginW,
		bodyBuf:        buff,
		gzWriter:       gzipW,
	}
}

func (w *Writer) WriteString(s string) (int, error) {
	w.Header().Del("Content-Length") // Fix: 这里很重要，一定要出之前的 Content-Length，确保后面压缩的 Content-Length 是正确的
	if count, err := w.bodyBuf.WriteString(s); err != nil {
		return count, err
	}
	return w.gzWriter.Write(utils.StringToBytes(s))
}

func (w *Writer) Write(b []byte) (int, error) {
	w.Header().Del("Content-Length") // Fix: 这里很重要，一定要出之前的 Content-Length，确保后面压缩的 Content-Length 是正确的
	if count, err := w.bodyBuf.Write(b); err != nil {
		return count, err
	}
	return w.gzWriter.Write(b)
}

// Fix: https://github.com/mholt/caddy/issues/38
func (w *Writer) WriteHeader(code int) {
	w.Header().Del("Content-Length") // Fix: 这里很重要，一定要出之前的 Content-Length，确保后面压缩的 Content-Length 是正确的
	w.ResponseWriter.WriteHeader(code)
}
