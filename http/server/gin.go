package server

import (
	"context"
	"errors"
	"fmt"
	"math"
	"net"
	"net/http"
	"sync"
	"time"

	boost "gitee.com/ayun2001/ok-boost-go"
	"gitee.com/ayun2001/ok-boost-go/http/server/common"
	"gitee.com/ayun2001/ok-boost-go/pkg/httptool"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/bwmarrin/snowflake"
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	ginSwaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"go.uber.org/zap"
)

var RouteNoRegisteredError = errors.New("router group not registered")

const serviceShutdownTimeoutSec = 10

type HttpServerConf struct {
	Address               string             `json:"address" yaml:"address"`
	Port                  uint16             `json:"port" yaml:"port"`
	HttpReadTimeout       uint32             `json:"httpReadTimeout,omitempty" yaml:"httpReadTimeout,omitempty"`
	HttpWriteTimeout      uint32             `json:"httpWriteTimeout,omitempty" yaml:"httpWriteTimeout,omitempty"`
	HttpReadHeaderTimeout uint32             `json:"httpReadHeaderTimeout,omitempty" yaml:"httpReadHeaderTimeout,omitempty"`
	Logger                *zap.SugaredLogger `json:"-" yaml:"-"`
}

type FeatureOpts struct {
	EnableGinRedirectTrailingSlash bool `json:"enableGinRedirectTrailingSlash,omitempty" yaml:"enableGinRedirectTrailingSlash,omitempty"` // 内部 301 路径跳转，如果当前路径的处理函数不存在，但是路径+'/'的处理函数存在，则允许进行重定向
	EnableGinRedirectFixedPath     bool `json:"enableGinRedirectFixedPath,omitempty" yaml:"enableGinRedirectFixedPath,omitempty"`         // 允许修复当前请求路径，如/FOO和/..//Foo会被修复为/foo，并进行重定向，默认为 false。
	EnableGinForwardedByClientIP   bool `json:"enableGinForwardedByClientIP,omitempty" yaml:"enableGinForwardedByClientIP,omitempty"`     // 如果应用程序不在代理之后，“ForwardedByClientIP”应设置为 false，因此“X-Forwarded-For”将被忽略。 如果在代理后面将其设置为true。
	EnableRecordContextBody        bool `json:"EnableRecordContextBody,omitempty" yaml:"EnableRecordContextBody,omitempty"`               // 记录会话 body 内容
	EnableHttpHealthCheck          bool `json:"enableHttpHealthCheck,omitempty" yaml:"enableHttpHealthCheck,omitempty"`                   // 健康检查
	EnableMetrics                  bool `json:"enableMetrics,omitempty" yaml:"enableMetrics,omitempty"`                                   // 记录 Prometheus 数据
	EnableSwagger                  bool `json:"enableSwagger,omitempty" yaml:"enableSwagger,omitempty"`                                   // Swagger
	EnableHttpPProf                bool `json:"enableHttpPProf,omitempty" yaml:"enableHttpPProf,omitempty"`                               // running time debug
}

type httpServiceInterface interface {
	RoutesRegistry(e *Engine)
}

type sharePool struct {
	requestBodyBufferPool  sync.Pool
	responseBodyBufferPool sync.Pool
	requestIdGeneratorPool sync.Pool
	requestMetadataPool    sync.Pool
}

type Engine struct {
	httpSvrEndpoint   string
	ginSvr            *gin.Engine
	rootRouteGroup    *gin.RouterGroup
	customRouteGroups map[string]*gin.RouterGroup
	lock              sync.RWMutex
	wg                sync.WaitGroup
	httpSvr           *http.Server
	httpConf          *HttpServerConf
	httpServices      []httpServiceInterface
	featureOpts       *FeatureOpts
	closeOnce         sync.Once
	serverMetrics     *serverMetrics
	handlers          *[]gin.HandlerFunc
	pool              *sharePool
}

func NewDefaultHttpServerConfig() *HttpServerConf {
	return &HttpServerConf{
		Address:               "127.0.0.1",
		Port:                  8080,
		HttpReadTimeout:       common.ConstHttpRequestIdleConnTimeout,
		HttpReadHeaderTimeout: common.ConstHttpRequestIdleConnTimeout,
		HttpWriteTimeout:      common.ConstHttpRequestIdleConnTimeout,
	}
}

func NewDefaultFeatureOpts() *FeatureOpts {
	return &FeatureOpts{
		EnableGinRedirectTrailingSlash: false,
		EnableGinRedirectFixedPath:     false,
		EnableGinForwardedByClientIP:   false,
		EnableRecordContextBody:        false,
		EnableHttpHealthCheck:          true,
		EnableMetrics:                  true,
		EnableSwagger:                  true,
		EnableHttpPProf:                true,
	}
}

func NewEngine(httpConf *HttpServerConf, featureOpts *FeatureOpts, middlewares *[]gin.HandlerFunc) *Engine {
	httpConf = validHttpServerConfig(httpConf)
	featureOpts = validFeatureOpts(featureOpts)
	middlewares = validHandlerFuncs(middlewares)

	pool := sharePool{
		requestBodyBufferPool: sync.Pool{New: func() interface{} {
			return common.NewContextBodyBuff()
		}},
		responseBodyBufferPool: sync.Pool{New: func() interface{} {
			return common.NewContextBodyBuff()
		}},
		requestIdGeneratorPool: sync.Pool{New: func() interface{} {
			snowflakeNode, _ := snowflake.NewNode(utils.CreateRand().Int63n(-1 ^ (-1 << snowflake.NodeBits)))
			return snowflakeNode
		}},
		requestMetadataPool: sync.Pool{New: func() interface{} {
			return newRequestMetadata()
		}},
	}

	// 创建引擎
	engine := &Engine{
		httpSvrEndpoint:   fmt.Sprintf("%s:%d", httpConf.Address, httpConf.Port),
		customRouteGroups: map[string]*gin.RouterGroup{},
		lock:              sync.RWMutex{},
		httpServices:      []httpServiceInterface{},
		wg:                sync.WaitGroup{},
		httpSvr:           &http.Server{},
		httpConf:          httpConf,
		featureOpts:       featureOpts,
		closeOnce:         sync.Once{},
		serverMetrics:     newHttpServerMetrics(),
		handlers:          middlewares,
		pool:              &pool,
	}

	// metrics 参数
	epMetricsOpts := map[string]interface{}{common.ConstPromServiceEndpoint: engine.httpSvrEndpoint} // metrics 扩展参数

	// 创建 gin 引擎
	gin.DisableConsoleColor() // 关闭终端彩色输出
	engine.ginSvr = gin.New()
	engine.ginSvr.ForwardedByClientIP = featureOpts.EnableGinForwardedByClientIP     // 获得真实的 client ip
	engine.ginSvr.RedirectTrailingSlash = featureOpts.EnableGinRedirectTrailingSlash // 传入是否需要路径跳转
	engine.ginSvr.RedirectFixedPath = featureOpts.EnableGinRedirectFixedPath
	engine.rootRouteGroup = &engine.ginSvr.RouterGroup

	// 自定义 gin 引擎内部错误
	engine.ginSvr.NoMethod(func(c *gin.Context) {
		c.JSON(http.StatusMethodNotAllowed, httptool.BaseHttpResponse{
			Code:         http.StatusMethodNotAllowed,
			ErrorMessage: http.StatusText(http.StatusMethodNotAllowed),
			ErrorDetail:  "http request method not allowed, method: " + c.Request.Method + ", path: " + c.Request.URL.Path,
		})
	})
	engine.ginSvr.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusNotFound, httptool.BaseHttpResponse{
			Code:         http.StatusNotFound,
			ErrorMessage: http.StatusText(http.StatusNotFound),
			ErrorDetail:  "http request route mismatch, method: " + c.Request.Method + ", path: " + c.Request.URL.Path,
		})
	})

	// 添加 health_check
	if featureOpts.EnableHttpHealthCheck {
		engine.rootRouteGroup.GET(common.ConstHttpHealthCheckUrlPath, func(c *gin.Context) {
			c.JSON(http.StatusOK, httptool.BaseHttpResponse{
				Code: httptool.HttpRequestOkCode, ErrorMessage: httptool.HttpRequestOk,
				ErrorDetail: "health-check is ok.",
			})
		})
	}

	// 添加 swagger
	if featureOpts.EnableSwagger {
		// http swagger
		engine.rootRouteGroup.GET(common.ConstHttpSwaggerUrlPath+"/*any", ginSwagger.WrapHandler(ginSwaggerFiles.Handler))
	}

	// 添加性能监控接口
	if featureOpts.EnableHttpPProf {
		httpProfService := NewHttpService(httpProfHandler)
		engine.HttpServiceRegistry(httpProfService)
	}

	// 添加 prometheus 监控接口
	if featureOpts.EnableMetrics {
		engine.serverMetrics.registerMetrics()
		engine.ginSvr.Use(engine.serverMetrics.HandlerFunc(&epMetricsOpts))
		engine.rootRouteGroup.GET(common.ConstPromMetricUrlPath, httptool.HttpHandlerToGinHandlerFunc(promhttp.Handler()))
	}

	// 全局基础组件, 其他的middleware 要依赖
	engine.ginSvr.Use(
		ginRequestMetaData(&pool.requestMetadataPool, &epMetricsOpts),
		ginResponseBodyBuffer(&pool.responseBodyBufferPool),
		ginRequestBodyBuffer(&pool.requestBodyBufferPool),
		ginRequestIdGenerator(&pool.requestIdGeneratorPool),
		ginSystemRecovery(), ginRequestID(), ginCors(),
	)

	// 返回对象
	return engine
}

func (e *Engine) initializeHttpSvrServices() {
	e.lock.RLock()
	defer e.lock.RUnlock()
	for _, service := range e.httpServices {
		service.RoutesRegistry(e)
	}
}

func (e *Engine) initializeHttpSvrHandlerFuncs() {
	// 注册自定义的中间件
	e.ginSvr.Use(*e.handlers...)

	// 注册日志输出模块
	e.ginSvr.Use(ginAccessLogger(e.httpConf.Logger, e.featureOpts.EnableRecordContextBody))
}

// GetHttpRegisteredGroup 获得已经注册的路由组
func (e *Engine) GetHttpRegisteredGroup(path string) (*gin.RouterGroup, error) {
	if path == common.ConstRootUrlPath {
		return &e.ginSvr.RouterGroup, nil
	}
	e.lock.RLock()
	defer e.lock.RUnlock()
	group, ok := e.customRouteGroups[path]
	if !ok {
		return nil, RouteNoRegisteredError
	}
	return group, nil
}

// HttpServiceRegistry 注册服务到 Engine
func (e *Engine) HttpServiceRegistry(services ...httpServiceInterface) {
	e.lock.Lock()
	e.httpServices = append(e.httpServices, services...)
	e.lock.Unlock()
}

// HttpRouteGroupRegistry 注册一个路由组
func (e *Engine) HttpRouteGroupRegistry(path string, baseRouteGroup *gin.RouterGroup) {
	if baseRouteGroup == nil {
		baseRouteGroup = &e.ginSvr.RouterGroup
	}
	e.lock.Lock()
	e.customRouteGroups[path] = baseRouteGroup.Group(path)
	e.lock.Unlock()
}

func (e *Engine) Start() {
	// 初始化 http server 的 middlewares
	e.initializeHttpSvrHandlerFuncs()

	// 初始化的路由
	e.initializeHttpSvrServices()

	// 初始化 http server
	e.httpSvr = &http.Server{
		Addr:              e.httpSvrEndpoint,
		Handler:           e.ginSvr,
		ReadTimeout:       time.Duration(e.httpConf.HttpReadTimeout) * time.Millisecond,
		ReadHeaderTimeout: time.Duration(e.httpConf.HttpReadHeaderTimeout) * time.Millisecond,
		WriteTimeout:      time.Duration(e.httpConf.HttpWriteTimeout) * time.Millisecond,
		IdleTimeout:       0, // 使用 HttpReadTimeout 做为这里的值
		MaxHeaderBytes:    math.MaxUint32,
		ErrorLog:          boost.StdLogger,
	}

	// 启动 http server
	e.wg.Add(1)
	go func() {
		defer e.wg.Done()
		boost.Logger.Infow("http server is ready", "address", e.httpSvrEndpoint)
		e.httpSvr.SetKeepAlivesEnabled(true) // 默认开启 keepalive， grpc/http 共享
		if err := e.httpSvr.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			boost.Logger.Fatalw("failed to start http server", "error", err)
		}
	}()
}

func (e *Engine) Stop() {
	e.closeOnce.Do(func() {
		// 关闭 http server
		ctx, cancel := context.WithTimeout(context.Background(), serviceShutdownTimeoutSec*time.Second)
		defer cancel()
		if e.httpSvr != nil {
			if err := e.httpSvr.Shutdown(ctx); err != nil {
				boost.Logger.Fatalw("http server forced to shutdown", "address", e.httpSvrEndpoint, "error", err)
			}
		}
		boost.Logger.Infow("http server is shutdown", "address", e.httpSvrEndpoint)

		// 等待结束
		e.wg.Wait()

		// 注销 serverMetrics
		if e.featureOpts.EnableMetrics {
			e.serverMetrics.unRegisterMetrics()
		}
	})
}

func validHttpServerConfig(conf *HttpServerConf) *HttpServerConf {
	if conf != nil {
		if ip := net.ParseIP(conf.Address); ip == nil {
			conf.Address = "127.0.0.1"
		}
		if conf.Port <= 0 {
			conf.Port = 8080
		}
		if conf.HttpReadTimeout <= 0 {
			conf.HttpReadTimeout = common.ConstHttpRequestIdleConnTimeout
		}
		if conf.HttpReadHeaderTimeout <= 0 {
			conf.HttpReadHeaderTimeout = common.ConstHttpRequestIdleConnTimeout
		}
		if conf.HttpWriteTimeout <= 0 {
			conf.HttpWriteTimeout = common.ConstHttpRequestIdleConnTimeout
		}
		if conf.Logger == nil {
			conf.Logger = boost.Logger
		}
	} else {
		conf = NewDefaultHttpServerConfig()
	}

	return conf
}

func validFeatureOpts(opts *FeatureOpts) *FeatureOpts {
	if opts != nil {
		return opts
	}

	return NewDefaultFeatureOpts()
}

func validHandlerFuncs(opts *[]gin.HandlerFunc) *[]gin.HandlerFunc {
	if opts != nil {
		return opts
	} else {
		handlers := make([]gin.HandlerFunc, 0)
		return &handlers
	}
}
