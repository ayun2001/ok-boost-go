package websocket

import (
	"errors"
	"sync"
	"time"

	boost "gitee.com/ayun2001/ok-boost-go"
	"gitee.com/ayun2001/ok-boost-go/http/server/common"
	"gitee.com/ayun2001/ok-boost-go/logger"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/gin-gonic/gin"
	"github.com/lxzan/gws"
	"github.com/panjf2000/ants/v2"
	"go.uber.org/zap"
)

const (
	defaultMaxWorkerCount        = -1 // 默认最大工作协程数，表示没有限制
	workerPoolStatusScanInterval = 5000
)

var (
	UpgraderIsNullError = errors.New("upgrader object is null")
	SocketIsNullError   = errors.New("socket object is null")
)

type FeatureOpts struct {
	EnableMetrics bool `json:"enableMetrics,omitempty" yaml:"enableMetrics,omitempty"` // 开启 metrics 记录
}

type WebSocketUpgrader struct {
	upgrader        *gws.Upgrader
	upgraderConf    *WebSocketUpgraderConfig
	featureOpts     *FeatureOpts
	upgraderMetrics *upgraderMetrics
	closeOnce       sync.Once
	workerPool      *ants.PoolWithFunc
}

type WebSocketUpgraderConfig struct {
	Logger *zap.SugaredLogger `json:"-" yaml:"-"`
}

func NewDefaultFeatureOpts() *FeatureOpts {
	return &FeatureOpts{
		EnableMetrics: true,
	}
}

func NewDefaultWebSocketUpgraderConfig() *WebSocketUpgraderConfig {
	return &WebSocketUpgraderConfig{}
}

func NewWebSocketUpgrader(upgrader *gws.Upgrader, upgraderConf *WebSocketUpgraderConfig, featureOpts *FeatureOpts) (*WebSocketUpgrader, error) {
	if upgrader == nil {
		return nil, UpgraderIsNullError
	}

	featureOpts = validFeatureOpts(featureOpts)
	upgraderConf = validWebSocketUpgraderConfig(upgraderConf)

	wsu := WebSocketUpgrader{
		upgrader:        upgrader,
		upgraderConf:    upgraderConf,
		featureOpts:     featureOpts,
		closeOnce:       sync.Once{},
		upgraderMetrics: newUpgraderMetrics(),
	}

	var err error
	wsu.workerPool, err = ants.NewPoolWithFunc(defaultMaxWorkerCount, wsu.recvMessageLoopWrapper, ants.WithOptions(ants.Options{
		ExpiryDuration: workerPoolStatusScanInterval * 3 * time.Second, // 每隔15秒自动回收休眠的 go routine
		PreAlloc:       true,
		Nonblocking:    false,
		Logger:         logger.ZapLoggerToStandardLogger(wsu.upgraderConf.Logger.Desugar()),
	}))
	if err != nil {
		return nil, err
	}

	if featureOpts.EnableMetrics {
		wsu.upgraderMetrics.registerMetrics()
	}

	return &wsu, nil
}

func (w *WebSocketUpgrader) Stop() {
	w.closeOnce.Do(func() {
		w.workerPool.Release()
		if w.featureOpts.EnableMetrics {
			w.upgraderMetrics.unRegisterMetrics()
		}
	})
}

// 参考文档：
// https://github.com/lxzan/gws
// https://studygolang.com/topics/16189
func (w *WebSocketUpgrader) Upgrade(c *gin.Context) (*gws.Conn, error) {
	reqMetadata := common.GetRequestMetadata(c)

	conn, err := w.upgrader.Upgrade(c.Writer, c.Request)
	if err != nil {
		w.upgraderConf.Logger.Errorw(
			"upgrade websocket failed",
			"requestID", c.GetHeader(common.ConstHttpRequestID),
			"clientIP", reqMetadata.ClientIP,
			"clientEndpoint", c.Request.RemoteAddr,
			"path", c.Request.URL.Path,
			"method", c.Request.Method,
			"userAgent", c.Request.UserAgent(),
			"error", err,
		)
		return nil, err
	}

	if w.featureOpts.EnableMetrics {
		// 记录数据
		path := c.Request.URL.Path
		method := c.Request.Method
		app := utils.ApplicationName

		w.upgraderMetrics.upgradeSocketCount.WithLabelValues(method, path, app, reqMetadata.Listener).Inc()
	}

	return conn, nil
}

func (w *WebSocketUpgrader) Submit(c *gws.Conn) error {
	if c != nil {
		return w.workerPool.Invoke(c)
	}

	return SocketIsNullError
}

func (w *WebSocketUpgrader) recvMessageLoopWrapper(data interface{}) {
	data.(*gws.Conn).ReadLoop()
}

func validWebSocketUpgraderConfig(conf *WebSocketUpgraderConfig) *WebSocketUpgraderConfig {
	if conf != nil {
		if conf.Logger == nil {
			conf.Logger = boost.Logger
		}
	} else {
		conf = NewDefaultWebSocketUpgraderConfig()
	}

	return conf
}

func validFeatureOpts(opts *FeatureOpts) *FeatureOpts {
	if opts != nil {
		return opts
	}

	return NewDefaultFeatureOpts()
}
