package websocket

import (
	"gitee.com/ayun2001/ok-boost-go/http/server/common"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/prometheus/client_golang/prometheus"
)

type upgraderMetrics struct {
	upgradeSocketCount *prometheus.CounterVec
}

func newUpgraderMetrics() *upgraderMetrics {
	return &upgraderMetrics{}
}

func (m *upgraderMetrics) registerMetrics() {
	var metricLabels = []string{"method", "path", "app", common.ConstPromServiceEndpoint}
	var id = utils.GetRandIdString()

	m.upgradeSocketCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "http_server_request_websocket_upgrade_count",
			Help:        "The total number of HTTP requests were upgraded to websocket.",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	prometheus.MustRegister(m.upgradeSocketCount)
}

func (m *upgraderMetrics) unRegisterMetrics() {
	prometheus.Unregister(m.upgradeSocketCount)
}
