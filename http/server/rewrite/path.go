package rewrite

import (
	"net/http"
	"sync"

	boost "gitee.com/ayun2001/ok-boost-go"
	"gitee.com/ayun2001/ok-boost-go/http/server/common"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

type PathRewriteConfig struct {
	PathRewriteFunc func(header *http.Request) (string, error) `json:"-" yaml:"-"`
	Logger          *zap.SugaredLogger                         `json:"-" yaml:"-"`
}

type PathRewrite struct {
	rewriterConf        *PathRewriteConfig
	pathRewriterMetrics *pathRewriterMetrics
	featureOpts         *FeatureOpts
	closeOnce           sync.Once
}

type FeatureOpts struct {
	EnableMetrics bool `json:"enableMetrics,omitempty" yaml:"enableMetrics,omitempty"` // 开启 metrics 记录
}

func NewDefaultFeatureOpts() *FeatureOpts {
	return &FeatureOpts{
		EnableMetrics: true,
	}
}

func NewDefaultPathRewriteConfig() *PathRewriteConfig {
	return &PathRewriteConfig{}
}

func NewPathRewrite(rewriterConf *PathRewriteConfig, featureOpts *FeatureOpts) *PathRewrite {
	featureOpts = validFeatureOpts(featureOpts)
	rewriterConf = validPathRewriteConfig(rewriterConf)

	pr := PathRewrite{
		rewriterConf:        rewriterConf,
		pathRewriterMetrics: newPathRewriteMetrics(),
		featureOpts:         featureOpts,
		closeOnce:           sync.Once{},
	}

	if featureOpts.EnableMetrics {
		pr.pathRewriterMetrics.registerMetrics()
	}

	return &pr
}

func (r *PathRewrite) Stop() {
	r.closeOnce.Do(func() {
		if r.featureOpts.EnableMetrics {
			r.pathRewriterMetrics.unRegisterMetrics()
		}
	})
}

func (r *PathRewrite) RewriteHandler() gin.HandlerFunc {
	// 返回一个匿名函数作为 gin.HandlerFunc
	return func(c *gin.Context) {
		// 如果请求资源需要跳过则继续后面的处理程序
		if common.SkipResources(c) {
			// 继续执行后面的处理程序
			c.Next()
			// 结束本次中间件的处理
			return
		}

		// 设置日志模块
		common.CompareAndSwapRequestLogger(c, r.rewriterConf.Logger)

		// 记录数据
		// 获取请求的路由，方法和应用名称
		path := c.Request.URL.Path
		method := c.Request.Method
		app := utils.ApplicationName
		reqMetadata := common.GetRequestMetadata(c)

		// 根据路由映射表替换 URL 中的路径
		if r.rewriterConf.PathRewriteFunc != nil {
			// 调用 PathRewriteFunc 函数进行路径重写
			if newPath, err := r.rewriterConf.PathRewriteFunc(c.Request); err != nil && newPath != "" {
				// 如果重写成功，则将新的路径设置到请求中
				c.Request.URL.Path = newPath
				// 如果启用了计量选项，则使用 pathRewriterMetrics 对象记录请求的统计指标
				if r.featureOpts.EnableMetrics {
					r.pathRewriterMetrics.rewriteCount.WithLabelValues(method, path, newPath, app, reqMetadata.Listener).Inc()
				}
				// 记录日志
				r.rewriterConf.Logger.Infow("path rewrite", "method", method, "path", path, "newPath", newPath, "app", app, "clientIP", reqMetadata.ClientIP, "listener", reqMetadata.Listener)
			}
		}

		// 调用下一个中间件处理程序
		c.Next()
	}
}

func validPathRewriteConfig(conf *PathRewriteConfig) *PathRewriteConfig {
	if conf != nil {
		if conf.Logger == nil {
			conf.Logger = boost.Logger
		}
	} else {
		conf = NewDefaultPathRewriteConfig()
	}

	return conf
}

func validFeatureOpts(opts *FeatureOpts) *FeatureOpts {
	if opts != nil {
		return opts
	}

	return NewDefaultFeatureOpts()
}
