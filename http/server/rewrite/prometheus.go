package rewrite

import (
	"gitee.com/ayun2001/ok-boost-go/http/server/common"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/prometheus/client_golang/prometheus"
)

type pathRewriterMetrics struct {
	rewriteCount *prometheus.CounterVec
}

func newPathRewriteMetrics() *pathRewriterMetrics {
	return &pathRewriterMetrics{}
}

func (p *pathRewriterMetrics) registerMetrics() {
	var metricLabels = []string{"method", "path", "rewritePath", "app", common.ConstPromServiceEndpoint}
	var id = utils.GetRandIdString()

	p.rewriteCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "http_server_request_path_rewrite_count",
			Help:        "The total number of HTTP request paths rewrited.",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	prometheus.MustRegister(p.rewriteCount)
}

func (p *pathRewriterMetrics) unRegisterMetrics() {
	prometheus.Unregister(p.rewriteCount)
}
