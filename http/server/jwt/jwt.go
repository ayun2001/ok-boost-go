package jwt

import (
	"fmt"
	"net/http"
	"strings"
	"sync"
	"time"

	"gitee.com/ayun2001/ok-boost-go/http/server/common"
	"gitee.com/ayun2001/ok-boost-go/pkg/httptool"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/gin-gonic/gin"
	jwt "github.com/golang-jwt/jwt/v4"
	"go.uber.org/zap"
)

const (
	ConstJwtExtMetadataKey            = "JWT_EXT_METADATA_DxesDCH7matc2VnPO7JuzE6s"
	ConstDefaultJwtTokenExpireSecords = 86400
)

type JsonWebToken struct {
	secretKey   []byte
	jwtConf     *JsonWebTokenConfig
	jwtMetrics  *jwtMetrics
	featureOpts *FeatureOpts
	closeOnce   sync.Once
}

type FeatureOpts struct {
	EnableMetrics bool `json:"enableMetrics,omitempty" yaml:"enableMetrics,omitempty"` // 开启 metrics 记录
}

type CreateTokenResponse struct {
	Token    string `json:"token,omitempty" yaml:"token,omitempty"`
	ExpireAt int64  `json:"expireAt,omitempty" yaml:"expireAt,omitempty"`
}

type JsonWebTokenConfig struct {
	TokenExpireSecords uint32                                      `json:"tokenExpireSecords,omitempty" yaml:"tokenExpireSecords,omitempty"`
	LoginAuthFunc      func() bool                                 `json:"-" yaml:"-"`
	GetExtMetadataFunc func(c *gin.Context) map[string]interface{} `json:"-" yaml:"-"`
	Logger             *zap.SugaredLogger                          `json:"-" yaml:"-"`
}

func NewDefaultFeatureOpts() *FeatureOpts {
	return &FeatureOpts{
		EnableMetrics: true,
	}
}

func NewDefaultJsonWebTokenConfig() *JsonWebTokenConfig {
	return &JsonWebTokenConfig{
		TokenExpireSecords: ConstDefaultJwtTokenExpireSecords,
		LoginAuthFunc: func() bool {
			return true
		},
		GetExtMetadataFunc: func(c *gin.Context) map[string]interface{} {
			return make(map[string]interface{}, 0)
		},
	}
}

func NewJsonWebToken(jwtConf *JsonWebTokenConfig, featureOpts *FeatureOpts) *JsonWebToken {
	jwtConf = validJsonWebTokenConfig(jwtConf)
	featureOpts = validFeatureOpts(featureOpts)

	jwt := JsonWebToken{
		secretKey:   utils.CreateRandomBytes(24),
		jwtConf:     jwtConf,
		jwtMetrics:  newJwtMetrics(),
		featureOpts: featureOpts,
		closeOnce:   sync.Once{},
	}

	if featureOpts.EnableMetrics {
		jwt.jwtMetrics.registerMetrics()
	}

	return &jwt
}

func (t *JsonWebToken) Stop() {
	t.closeOnce.Do(func() {
		if t.featureOpts.EnableMetrics {
			t.jwtMetrics.unRegisterMetrics()
		}
	})
}

func (t *JsonWebToken) validToken(tokenString string) (map[string]interface{}, bool, error) {
	// 解析 token
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// 验证 token 的签名算法是否是 HS256
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return t.secretKey, nil
	})

	if err != nil {
		return nil, false, err
	}

	// 如果 token 有效
	if token.Valid {
		// 获取 claims
		claims := token.Claims.(jwt.MapClaims)
		// 获取过期时间
		expireAt := int64(claims["expireAt"].(float64))
		// 如果过期时间小于当前时间
		if expireAt <= time.Now().Unix() {
			return nil, false, nil
		}

		// 获取额外信息
		extMetadata, ok := claims[ConstJwtExtMetadataKey].(map[string]interface{})
		if !ok {
			return nil, false, nil
		}

		return extMetadata, true, nil
	}

	return nil, false, nil
}

func (t *JsonWebToken) createToken(c *gin.Context) (string, int64, error) {
	// 计算 token 到期时间
	expireAt := time.Now().Add(time.Second * time.Duration(t.jwtConf.TokenExpireSecords)).Unix()
	// 创建 jwt 对象
	token := jwt.New(jwt.SigningMethodHS256)
	// 将 claims 断言为 MapClaims 类型，用于设置信息
	claims := token.Claims.(jwt.MapClaims)
	// 设置过期时间
	claims["expireAt"] = expireAt

	// 设置额外信息
	if t.jwtConf.GetExtMetadataFunc != nil {
		claims[ConstJwtExtMetadataKey] = t.jwtConf.GetExtMetadataFunc(c)
	}

	// 生成 token
	tokenString, err := token.SignedString(t.secretKey)
	if err != nil {
		return "", -1, err
	}

	return tokenString, expireAt, nil
}

func (t *JsonWebToken) vaildJwtConfig(c *gin.Context) bool {
	if t.jwtConf.LoginAuthFunc == nil {
		c.JSON(http.StatusInternalServerError, httptool.BaseHttpResponse{
			Code:         http.StatusInternalServerError,
			ErrorMessage: "Faild to valid JWT config",
			ErrorDetail:  "LoginAuthFunc is nil, can not create token",
		})
		return false
	}

	return true
}

func (t *JsonWebToken) createTokenHandler() gin.HandlerFunc {
	// 返回一个 gin 处理函数
	return func(c *gin.Context) {
		// 验证 jwt 配置是否有效
		if !t.vaildJwtConfig(c) {
			return
		}

		// 日志模块
		common.CompareAndSwapRequestLogger(c, t.jwtConf.Logger)

		// 记录数据
		path := c.Request.URL.Path
		method := c.Request.Method
		app := utils.ApplicationName
		reqMetadata := common.GetRequestMetadata(c)

		// 如果登录认证不成功
		if !t.jwtConf.LoginAuthFunc() {
			// 如果登录认证失败，返回相应状态码以及错误信息
			c.JSON(http.StatusUnauthorized, httptool.BaseHttpResponse{
				Code:         http.StatusUnauthorized,
				ErrorMessage: "Unauthorized",
				ErrorDetail:  "Invalid credentials",
			})
			// 如果开启了 metrics，记录登录认证失败的次数
			if t.featureOpts.EnableMetrics {
				t.jwtMetrics.createFailCount.WithLabelValues(method, path, app, reqMetadata.Listener).Inc()
			}
			// 记录日志
			t.jwtConf.Logger.Warnw("Failed to login auth", "method", method, "clientIP", reqMetadata.ClientIP, "path", path, "app", app, "listener", reqMetadata.Listener)
			// 返回
			return
		}

		// 生成 JWT token 字符串和过期时间 Unix 时间戳
		tokenString, expireAt, err := t.createToken(c)
		if err != nil {
			// 如果生成 token 出错，返回相应状态码以及错误信息
			c.JSON(http.StatusInternalServerError, httptool.BaseHttpResponse{
				Code:         http.StatusInternalServerError,
				ErrorMessage: "Failed to generate token",
				ErrorDetail:  err.Error(),
			})
			// 如果开启了 metrics，记录生成 token 出错的次数
			if t.featureOpts.EnableMetrics {
				t.jwtMetrics.createFailCount.WithLabelValues(method, path, app, reqMetadata.Listener).Inc()
			}
			// 记录日志
			t.jwtConf.Logger.Errorw("Failed to create token", "method", method, "clientIP", reqMetadata.ClientIP, "path", path, "app", app, "listener", reqMetadata.Listener, "error", err)
			// 返回
			return
		}

		// 成功生成 token 后返回成功状态码以及包含 token 信息的响应体
		c.JSON(http.StatusOK, httptool.BaseHttpResponse{
			Code:         http.StatusOK,
			ErrorMessage: "OK",
			Data:         CreateTokenResponse{Token: tokenString, ExpireAt: expireAt},
		})
	}
}

func (t *JsonWebToken) getTokenStringFromHeader(c *gin.Context) (string, bool) {
	authHeaderPrefix := "Bearer "
	authHeader := c.GetHeader("Authorization")
	if strings.HasPrefix(authHeader, authHeaderPrefix) {
		return authHeader[len(authHeaderPrefix):], true
	} else {
		return authHeader, true
	}
}

func (t *JsonWebToken) RouteHandler(rg *gin.RouterGroup) {
	rg.POST(common.ConstHttpJwtCreateTokenUrlPath, t.createTokenHandler())
}

func (t *JsonWebToken) JwtAuthenticator() gin.HandlerFunc {
	return func(c *gin.Context) {
		// 路径过滤器，判断当前的路径是否需要进行 JWT 验证，
		// 如果不需要直接跳过该 HandlerFunc
		if common.SkipResources(c) {
			c.Next()
			return
		}

		// 记录数据
		path := c.Request.URL.Path
		method := c.Request.Method
		app := utils.ApplicationName
		reqMetadata := common.GetRequestMetadata(c)

		// 日志模块
		common.CompareAndSwapRequestLogger(c, t.jwtConf.Logger)

		// 获取请求头 Authorization 中的 token 字符串
		tokenString, ok := t.getTokenStringFromHeader(c)
		if !ok {
			// 如果没有提供 Authorization 请求头，返回未授权的错误信息
			c.AbortWithStatusJSON(http.StatusUnauthorized, httptool.BaseHttpResponse{
				Code:         http.StatusUnauthorized,
				ErrorMessage: "Unauthorized",
				ErrorDetail:  "Authorization header is missing OR formating is incorrect",
			})
			// 记录数据
			if t.featureOpts.EnableMetrics {
				t.jwtMetrics.validFailCount.WithLabelValues(method, path, app, reqMetadata.Listener).Inc()
			}
			// 记录日志
			t.jwtConf.Logger.Warnw("Authorization header is missing OR formating is incorrect", "method", method, "clientIP", reqMetadata.ClientIP, "path", path, "app", app, "listener", reqMetadata.Listener)
			// 返回
			return
		}

		// 验证 token 的合法性
		extMetadata, ok, err := t.validToken(tokenString)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, httptool.BaseHttpResponse{
				Code:         http.StatusBadRequest,
				ErrorMessage: "Failed to detect the jwt token",
				ErrorDetail:  err.Error(),
			})
			// 记录数据
			if t.featureOpts.EnableMetrics {
				t.jwtMetrics.validFailCount.WithLabelValues(method, path, app, reqMetadata.Listener).Inc()
			}
			// 记录日志
			t.jwtConf.Logger.Errorw("Failed to detect the jwt token", "method", method, "clientIP", reqMetadata.ClientIP, "path", path, "app", app, "listener", reqMetadata.Listener, "error", err)
			// 返回
			return
		}

		if !ok {
			c.AbortWithStatusJSON(http.StatusBadRequest, httptool.BaseHttpResponse{
				Code:         http.StatusBadRequest,
				ErrorMessage: "Failed to detect the jwt token",
				ErrorDetail:  "Invalid token or token expired",
			})
			// 记录数据
			if t.featureOpts.EnableMetrics {
				t.jwtMetrics.validFailCount.WithLabelValues(method, path, app, reqMetadata.Listener).Inc()
			}
			// 记录日志
			t.jwtConf.Logger.Errorw("Invalid token or token expired", "method", method, "clientIP", reqMetadata.ClientIP, "path", path, "app", app, "listener", reqMetadata.Listener)
			// 返回
			return
		}

		// 将 JWT token 缓存为一个键值对映射，此处列出其它内容（如用户ID）的扩展元数据
		if extMetadata != nil {
			c.Set(ConstJwtExtMetadataKey, extMetadata)
		}

		// 如果 JWT token 验证通过，调用接下来的中间件或者路由处理器
		c.Next()

		// 处理完后清除 JWT token 缓存
		if extMetadata != nil {
			c.Set(ConstJwtExtMetadataKey, nil)
		}
	}
}

func validJsonWebTokenConfig(conf *JsonWebTokenConfig) *JsonWebTokenConfig {
	if conf != nil {
		if conf.TokenExpireSecords <= 0 {
			conf.TokenExpireSecords = ConstDefaultJwtTokenExpireSecords
		}
	} else {
		conf = NewDefaultJsonWebTokenConfig()
	}

	return conf
}

func validFeatureOpts(opts *FeatureOpts) *FeatureOpts {
	if opts != nil {
		return opts
	}

	return NewDefaultFeatureOpts()
}
