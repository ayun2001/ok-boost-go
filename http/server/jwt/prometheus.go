package jwt

import (
	"gitee.com/ayun2001/ok-boost-go/http/server/common"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/prometheus/client_golang/prometheus"
)

type jwtMetrics struct {
	createFailCount *prometheus.CounterVec
	validFailCount  *prometheus.CounterVec
}

func newJwtMetrics() *jwtMetrics {
	return &jwtMetrics{}
}

func (m *jwtMetrics) registerMetrics() {
	var metricLabels = []string{"method", "path", "app", common.ConstPromServiceEndpoint}
	var id = utils.GetRandIdString()

	m.createFailCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "jwt_create_failed_events_count",
			Help:        "The total number of failed JWT created",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	m.validFailCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "jwt_valid_failed_events_count",
			Help:        "The total number of failed JWT validated",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	prometheus.MustRegister(m.createFailCount, m.validFailCount)
}

func (m *jwtMetrics) unRegisterMetrics() {
	prometheus.Unregister(m.createFailCount)
	prometheus.Unregister(m.validFailCount)
}
