package ratelimiter

import (
	"context"
	"net"
	"net/http"
	"sort"
	"sync"
	"time"

	boost "gitee.com/ayun2001/ok-boost-go"
	"gitee.com/ayun2001/ok-boost-go/http/server/common"
	"gitee.com/ayun2001/ok-boost-go/pkg/httptool"
	"gitee.com/ayun2001/ok-boost-go/pkg/timer"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

const (
	segmentCount    = uint32(1 << 8)
	segmentAndOpVal = segmentCount - 1
)

type ipRateLimiterElement struct {
	limiter  *RateLimiter
	updateAt int64
}

type ipRateLimiterSegement struct {
	items      map[uint32]*ipRateLimiterElement
	lock       sync.Mutex
	timer      *timer.CachedTimer
	stopCtx    context.Context
	stopCancel context.CancelFunc
	wg         sync.WaitGroup
	closeOnce  sync.Once // 保证计时器只关闭一次
}

func newIpRateLimiterSegement() *ipRateLimiterSegement {
	seg := ipRateLimiterSegement{
		items:     map[uint32]*ipRateLimiterElement{},
		lock:      sync.Mutex{},
		timer:     timer.NewCachedTimer(),
		wg:        sync.WaitGroup{},
		closeOnce: sync.Once{},
	}

	seg.stopCtx, seg.stopCancel = context.WithCancel(context.Background())

	// 定时清理过期的限速器
	seg.wg.Add(1)
	go seg.evacuate()

	return &seg
}

func (s *ipRateLimiterSegement) GetOrCreate(key uint32, conf *RateLimiterConfig, featureOpts *FeatureOpts) *RateLimiter {
	s.lock.Lock()
	defer s.lock.Unlock()

	if rl, ok := s.items[key]; ok && rl != nil {
		rl.updateAt = s.timer.Now() // 更新访问时间
		return rl.limiter
	} else {
		rl = &ipRateLimiterElement{
			limiter: NewRateLimiter(
				&RateLimiterConfig{
					Rate:                       conf.Rate,
					Capacity:                   conf.Capacity,
					Logger:                     conf.Logger,
					HttpRequestHeaderMatchFunc: conf.HttpRequestHeaderMatchFunc,
				},
				featureOpts,
			),
			updateAt: s.timer.Now(),
		}

		s.items[key] = rl
		return rl.limiter
	}
}

func (s *ipRateLimiterSegement) evacuate() {
	const (
		expriseSecords        = 60 * 15 // 15分钟
		tickerIntervalSecords = 30      // 30秒
	)

	ticker := time.NewTicker(time.Second * tickerIntervalSecords)

	defer func() {
		ticker.Stop()
		s.wg.Done()
	}()

	for {
		select {
		case <-s.stopCtx.Done(): // 如果 stopCtx 被取消，结束循环
			return
		case <-ticker.C:
			// 清理长时间空置的限速器
			s.lock.Lock()
			for key, item := range s.items {
				if s.timer.Now()-item.updateAt >= expriseSecords {
					item.limiter.Stop() // 关闭限速器, 别忘记了
					delete(s.items, key)
				}
			}
			s.lock.Unlock()
		}
	}
}

func (s *ipRateLimiterSegement) Stop() {
	s.closeOnce.Do(func() {
		s.stopCancel()
		s.wg.Wait()
		s.timer.Stop()
	})
}

type IpRateLimiterConfig struct {
	Rate                       uint32                     `json:"rate,omitempty" yaml:"rate,omitempty"`
	Capacity                   uint32                     `json:"capacity,omitempty" yaml:"capacity,omitempty"`
	WhiteList                  []string                   `json:"whiteList,omitempty" yaml:"whiteList,omitempty"`
	HttpRequestHeaderMatchFunc func(h *http.Request) bool `json:"-" yaml:"-"` // 匹配请求头，指定特定的请求 Header 中信息做匹配，如果匹配，启动限速器。
	Logger                     *zap.SugaredLogger         `json:"-" yaml:"-"`
}

type IpRateLimiter struct {
	segments     []*ipRateLimiterSegement
	limiterConf  *IpRateLimiterConfig
	featureOpts  *FeatureOpts
	closeOnce    sync.Once
	whitelist    []*ipRange
	queryIpCache sync.Map
}

func NewDefaultIpRateLimiterConfig() *IpRateLimiterConfig {
	return &IpRateLimiterConfig{
		Rate:      ConstDefaultLimiteRate,
		Capacity:  ConstDefaultLimiteCapacity,
		WhiteList: make([]string, 0),
	}
}

func NewIpRateLimiter(limiterConf *IpRateLimiterConfig, featureOpts *FeatureOpts) *IpRateLimiter {
	featureOpts = validFeatureOpts(featureOpts)
	limiterConf = validIpRateLimiterConfig(limiterConf)

	rl := IpRateLimiter{
		limiterConf:  limiterConf,
		featureOpts:  featureOpts,
		closeOnce:    sync.Once{},
		queryIpCache: sync.Map{},
	}

	// 初始化限速器
	rl.segments = make([]*ipRateLimiterSegement, segmentCount)
	for i := uint32(0); i < segmentCount; i++ {
		rl.segments[i] = newIpRateLimiterSegement()
	}

	// 初始化白名单
	rl.initialExclusionList()

	return &rl
}

func (r *IpRateLimiter) Stop() {
	r.closeOnce.Do(func() {
		// 此时内存可能非常大，这样的 for 不复制对象
		for i := uint32(0); i < segmentCount; i++ {
			r.segments[i].Stop()
		}
	})
}

// Token 限速器
func (r *IpRateLimiter) TokenLimiter() gin.HandlerFunc {
	rlconf := RateLimiterConfig{
		Rate:                       r.limiterConf.Rate,
		Capacity:                   r.limiterConf.Capacity,
		Logger:                     r.limiterConf.Logger,
		HttpRequestHeaderMatchFunc: r.limiterConf.HttpRequestHeaderMatchFunc,
	}

	// 返回处理程序
	return func(c *gin.Context) {
		if common.SkipResources(c) {
			// 继续执行后面的处理程序
			c.Next()
			return
		}

		// 如果请求头匹配函数不为空，则执行请求头匹配函数
		if r.limiterConf.HttpRequestHeaderMatchFunc != nil {
			// 如果请求头匹配函数返回false，则继续执行后面的处理程序
			if !r.limiterConf.HttpRequestHeaderMatchFunc(c.Request) {
				// 继续执行后面的处理程序
				c.Next()
				return
			}
		}

		// 获取客户端 Request metadata, 从中获取客户端IP
		reqMetadata := common.GetRequestMetadata(c)
		targetIp := net.ParseIP(reqMetadata.ClientIP) // 这里可以用 map 优化
		ipNum := ipToUint32(&targetIp)

		// 如果客户端IP被白名单命中，则继续执行后面的处理程序
		if binarySearch(r.whitelist, ipNum) {
			// 继续执行后面的处理程序
			c.Next()
			return
		}

		// 设置日志模块
		common.CompareAndSwapRequestLogger(c, r.limiterConf.Logger)

		// 获得IP对应的限速器，
		hashIdx := ipNum & segmentAndOpVal
		// 获取限速器, 如果不存在则创建
		limiter := r.segments[hashIdx].GetOrCreate(ipNum, &rlconf, r.featureOpts)
		// 如果请求超过速率限制
		if limiter.rl.TakeAvailable(1) == 0 {
			// 请求超过速率限制
			c.AbortWithStatusJSON(http.StatusTooManyRequests, httptool.BaseHttpResponse{
				Code:         http.StatusTooManyRequests,
				ErrorMessage: "Too many requests, exceeds rate limit, aborted.",
			})

			// 记录数据
			path := c.Request.URL.Path
			method := c.Request.Method
			app := utils.ApplicationName

			limiter.limterMetrics.limitCount.WithLabelValues(method, path, app, reqMetadata.Listener).Inc()
			limiter.limiterConf.Logger.Warnw("request limit", "method", method, "clientIP", reqMetadata.ClientIP, "path", path, "app", app, "listener", reqMetadata.Listener)

			// 请求被终止，不再执行后面的处理程序
			return
		}

		// 继续执行后面的处理程序
		c.Next()
	}
}

// 初始化IP列表白名单
func (r *IpRateLimiter) initialExclusionList() {
	// 去重
	ipNetList := utils.RemoveDuplicatesAndEmptyS(r.limiterConf.WhiteList)

	// 将IP地址段转换为整数范围
	ipRangeList := make([]*ipRange, 0, len(ipNetList))
	for _, ipNet := range ipNetList {
		_, cidr, err := net.ParseCIDR(ipNet)
		if err != nil {
			r.limiterConf.Logger.Errorw("ip rate limiter parse cidr", "subnet", ipNet, "error", err.Error())
			continue
		}

		ipRange := ipRange{}
		ipRange.startIP, ipRange.endIP = ipRangeFromCIDR(cidr)
		ipRangeList = append(ipRangeList, &ipRange)
	}

	// 对IP地址段进行排序
	sort.Slice(ipRangeList, func(i, j int) bool {
		return ipRangeList[i].startIP < ipRangeList[j].startIP
	})

	// 将IP地址段列表设置到白名单中
	r.whitelist = ipRangeList
}

type ipRange struct {
	startIP uint32
	endIP   uint32
}

func ipRangeFromCIDR(ipNet *net.IPNet) (uint32, uint32) {
	startIP := ipNetToUInt32(ipNet)
	mask := binaryToLength(ipNet.Mask)
	endIP := startIP | (1<<(32-mask) - 1)
	return startIP, endIP
}

func ipNetToUInt32(ipNet *net.IPNet) uint32 {
	ipSlice := ipNet.IP.To4()
	return (uint32(ipSlice[0]) << 24) | (uint32(ipSlice[1]) << 16) | (uint32(ipSlice[2]) << 8) | uint32(ipSlice[3])
}

func ipToUint32(ip *net.IP) uint32 {
	ipSlice := ip.To4()
	return (uint32(ipSlice[0]) << 24) | (uint32(ipSlice[1]) << 16) | (uint32(ipSlice[2]) << 8) | uint32(ipSlice[3])
}

func binaryToLength(binaryMask net.IPMask) uint32 {
	bits, _ := binaryMask.Size()
	return uint32(bits)
}

// 二分查找
func binarySearch(ipRanges []*ipRange, targetIP uint32) bool {
	left, right := int64(0), int64(len(ipRanges)-1) // 定义左右边界

	for left <= right { // 循环直到找到目标区间或搜索完所有区间
		mid := (left + right) >> 1 // 计算中间位置

		if ipRanges[mid].startIP <= targetIP && targetIP <= ipRanges[mid].endIP { // 目标 IP 在当前区间内
			return true
		} else if ipRanges[mid].endIP < targetIP { // 目标 IP 在当前区间右侧，缩小搜索范围
			left = mid + 1
		} else { // 目标 IP 在当前区间左侧，缩小搜索范围
			right = mid - 1
		}
	}

	return false // 目标 IP 不在任何区间内
}

func validIpRateLimiterConfig(conf *IpRateLimiterConfig) *IpRateLimiterConfig {
	if conf != nil {
		if conf.Rate <= 0 {
			conf.Rate = ConstDefaultLimiteRate
		}
		if conf.Capacity <= 0 {
			conf.Capacity = ConstDefaultLimiteCapacity
		}
		if conf.WhiteList == nil {
			conf.WhiteList = make([]string, 0)
		}
		if conf.Logger == nil {
			conf.Logger = boost.Logger
		}
	} else {
		conf = NewDefaultIpRateLimiterConfig()
	}

	return conf
}
