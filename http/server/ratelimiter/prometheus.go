package ratelimiter

import (
	"gitee.com/ayun2001/ok-boost-go/http/server/common"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/prometheus/client_golang/prometheus"
)

type limterMetrics struct {
	limitCount      *prometheus.CounterVec
	bucketRate      *prometheus.GaugeVec
	availableTokens *prometheus.GaugeVec
}

// Available returns the number of available tokens

func newLimterMetrics() *limterMetrics {
	return &limterMetrics{}
}

func (m *limterMetrics) registerMetrics() {
	var metricLabels = []string{"method", "path", "app", common.ConstPromServiceEndpoint}
	var id = utils.GetRandIdString()

	m.limitCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "http_server_request_ratelimit_count",
			Help:        "The total number of HTTP requests are restricted.",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	m.bucketRate = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "http_server_request_ratelimit_bucket_fill_rate",
			Help:        "The fill rate of the bucket, in tokens per second.",
			ConstLabels: map[string]string{"id": id},
		}, []string{"app"},
	)

	m.availableTokens = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "http_server_request_ratelimit_available_tokens",
			Help:        "The total number of available tokens.",
			ConstLabels: map[string]string{"id": id},
		}, []string{"app"},
	)

	prometheus.MustRegister(m.limitCount, m.bucketRate, m.availableTokens)
}

func (m *limterMetrics) unRegisterMetrics() {
	prometheus.Unregister(m.limitCount)
	prometheus.Unregister(m.bucketRate)
	prometheus.Unregister(m.availableTokens)
}
