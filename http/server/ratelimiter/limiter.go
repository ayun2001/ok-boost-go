package ratelimiter

import (
	"context"
	"net/http"
	"sync"
	"time"

	boost "gitee.com/ayun2001/ok-boost-go"
	"gitee.com/ayun2001/ok-boost-go/http/server/common"
	"gitee.com/ayun2001/ok-boost-go/pkg/httptool"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/gin-gonic/gin"
	"github.com/juju/ratelimit"
	"go.uber.org/zap"
)

const (
	ConstDefaultLimiteRate     = 10
	ConstDefaultLimiteCapacity = 50
)

type RateLimiterConfig struct {
	Rate                       uint32                     `json:"rate,omitempty" yaml:"rate,omitempty"`
	Capacity                   uint32                     `json:"capacity,omitempty" yaml:"capacity,omitempty"`
	Logger                     *zap.SugaredLogger         `json:"-" yaml:"-"`
	HttpRequestHeaderMatchFunc func(h *http.Request) bool `json:"-" yaml:"-"` // 匹配请求头，指定特定的请求 Header 中信息做匹配，如果匹配，启动限速器。
}

type RateLimiter struct {
	rl            *ratelimit.Bucket
	limterMetrics *limterMetrics
	limiterConf   *RateLimiterConfig
	featureOpts   *FeatureOpts
	closeOnce     sync.Once
	stopCtx       context.Context
	stopCancel    context.CancelFunc
	wg            sync.WaitGroup
}

type FeatureOpts struct {
	EnableMetrics bool `json:"enableMetrics,omitempty" yaml:"enableMetrics,omitempty"` // 开启 metrics 记录
}

func NewDefaultFeatureOpts() *FeatureOpts {
	return &FeatureOpts{
		EnableMetrics: true,
	}
}

func NewDefaultRateLimiterConfig() *RateLimiterConfig {
	return &RateLimiterConfig{
		Rate:     ConstDefaultLimiteRate,
		Capacity: ConstDefaultLimiteCapacity,
	}
}

// 每秒钟处理10个请求，并且允许同时处理5个请求
// 也就是说，每秒钟处理10个请求，但是允许同时处理5个请求，超过5个请求的请求会被限制
// ratelimit.NewBucketWithRate(10, 5)
func NewRateLimiter(limiterConf *RateLimiterConfig, featureOpts *FeatureOpts) *RateLimiter {
	featureOpts = validFeatureOpts(featureOpts)
	limiterConf = validRateLimiterConfig(limiterConf)

	rl := RateLimiter{
		rl:            ratelimit.NewBucketWithRate(float64(limiterConf.Rate), int64(limiterConf.Capacity)),
		limterMetrics: newLimterMetrics(),
		limiterConf:   limiterConf,
		featureOpts:   featureOpts,
		closeOnce:     sync.Once{},
		wg:            sync.WaitGroup{},
	}

	rl.stopCtx, rl.stopCancel = context.WithCancel(context.Background())

	if featureOpts.EnableMetrics {
		rl.limterMetrics.registerMetrics()
	}

	rl.wg.Add(1)
	go rl.statusSyncer()

	return &rl
}

func (r *RateLimiter) Stop() {
	r.closeOnce.Do(func() {
		r.stopCancel()
		r.wg.Wait()
		if r.featureOpts.EnableMetrics {
			r.limterMetrics.unRegisterMetrics()
		}
	})
}

// Token 限速器
func (r *RateLimiter) TokenLimiter() gin.HandlerFunc {
	// Gin框架的中间件函数，作用是限制请求速率
	return func(c *gin.Context) {
		if common.SkipResources(c) {
			// 继续执行后面的处理程序
			c.Next()
			return
		}

		// 如果请求头匹配函数不为空，则执行请求头匹配函数
		if r.limiterConf.HttpRequestHeaderMatchFunc != nil {
			// 如果请求头匹配函数返回false，则继续执行后面的处理程序
			if !r.limiterConf.HttpRequestHeaderMatchFunc(c.Request) {
				// 继续执行后面的处理程序
				c.Next()
				return
			}
		}

		// 设置日志模块
		common.CompareAndSwapRequestLogger(c, r.limiterConf.Logger)

		// 如果请求超过速率限制
		if r.rl.TakeAvailable(1) == 0 {
			// 请求超过速率限制
			c.AbortWithStatusJSON(http.StatusTooManyRequests, httptool.BaseHttpResponse{
				Code:         http.StatusTooManyRequests,
				ErrorMessage: "Too many requests, exceeds rate limit, aborted.",
			})

			// 记录数据
			path := c.Request.URL.Path
			method := c.Request.Method
			app := utils.ApplicationName
			reqMetadata := common.GetRequestMetadata(c)

			r.limterMetrics.limitCount.WithLabelValues(method, path, app, reqMetadata.Listener).Inc()
			r.limiterConf.Logger.Warnw("request limit", "method", method, "clientIP", reqMetadata.ClientIP, "path", path, "app", app, "listener", reqMetadata.Listener)

			// 请求被终止，不再执行后面的处理程序
			return
		}

		// 继续执行后面的处理程序
		c.Next()
	}
}

func (r *RateLimiter) statusSyncer() {
	ticker := time.NewTicker(time.Second * 5) // 每5秒钟记录一次数据
	app := utils.ApplicationName

	defer func() {
		ticker.Stop()
		r.wg.Done()
	}()

	for {
		select {
		case <-ticker.C:
			// 记录数据
			r.limterMetrics.bucketRate.WithLabelValues(app).Set(r.rl.Rate())
			r.limterMetrics.availableTokens.WithLabelValues(app).Set(float64(r.rl.Available()))
		case <-r.stopCtx.Done(): // 如果 stopCtx 被取消，结束循环
			return
		}
	}
}

func validRateLimiterConfig(conf *RateLimiterConfig) *RateLimiterConfig {
	if conf != nil {
		if conf.Rate <= 0 {
			conf.Rate = ConstDefaultLimiteRate
		}
		if conf.Capacity <= 0 {
			conf.Capacity = ConstDefaultLimiteCapacity
		}
		if conf.Logger == nil {
			conf.Logger = boost.Logger
		}
	} else {
		conf = NewDefaultRateLimiterConfig()
	}

	return conf
}

func validFeatureOpts(opts *FeatureOpts) *FeatureOpts {
	if opts != nil {
		return opts
	}

	return NewDefaultFeatureOpts()
}
