package server

import (
	"strconv"
	"time"

	"gitee.com/ayun2001/ok-boost-go/http/server/common"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
)

type serverMetrics struct {
	requestCount     *prometheus.CounterVec
	requestLatencies *prometheus.HistogramVec
	requestLatency   *prometheus.GaugeVec
}

func newHttpServerMetrics() *serverMetrics {
	return &serverMetrics{}
}

func (m *serverMetrics) registerMetrics() {
	var metricLabels = []string{"status", "method", "path", "app", common.ConstPromServiceEndpoint}
	var id = utils.GetRandIdString()

	m.requestCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "http_server_request_count",
			Help:        "The total number of HTTP requests made.",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	m.requestLatency = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "http_server_request_latency_seconds",
			Help:        "HTTP request latencies in seconds.",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	m.requestLatencies = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "http_server_request_latency",
			Help:        "HTTP request latencies in seconds.",
			ConstLabels: map[string]string{"id": id},
		}, []string{"app", common.ConstPromServiceEndpoint},
	)

	prometheus.MustRegister(m.requestCount, m.requestLatencies, m.requestLatency)
}

func (m *serverMetrics) unRegisterMetrics() {
	prometheus.Unregister(m.requestCount)
	prometheus.Unregister(m.requestLatency)
	prometheus.Unregister(m.requestLatencies)
}

// HandlerFunc returns a gin.HandlerFunc for exporting some Web metrics
func (m *serverMetrics) HandlerFunc(extOpts *map[string]interface{}) gin.HandlerFunc {
	endpoint := (*extOpts)[common.ConstPromServiceEndpoint].(string)

	return func(c *gin.Context) {
		start := time.Now()

		// 下一个
		c.Next()

		if common.SkipResources(c) {
			return
		}

		// 记录数据
		status := strconv.Itoa(c.Writer.Status())
		path := c.Request.URL.Path
		method := c.Request.Method
		app := utils.ApplicationName
		latency := time.Since(start).Seconds()

		m.requestCount.WithLabelValues(status, method, path, app, endpoint).Inc()
		m.requestLatency.WithLabelValues(status, method, path, app, endpoint).Set(latency)
		m.requestLatencies.WithLabelValues(app, endpoint).Observe(latency)
	}
}
