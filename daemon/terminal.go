package signal

import (
	"context"
	"sync"
	"time"
)

const ConstInfinityTerminateTimeout = time.Duration(-1)

type TerminateSignal struct {
	ctx       context.Context
	cancel    context.CancelFunc
	wg        sync.WaitGroup
	callbacks []func()
	closeOnce sync.Once
}

func NewTerminateSignalWithContext(ctx context.Context, timeout time.Duration) *TerminateSignal {
	t := TerminateSignal{
		wg:        sync.WaitGroup{},
		callbacks: []func(){},
		closeOnce: sync.Once{},
	}

	if int64(timeout) <= 0 {
		t.ctx, t.cancel = context.WithCancel(ctx)
	} else {
		t.ctx, t.cancel = context.WithTimeout(ctx, timeout)
	}

	return &t
}

func NewTerminateSignal(timeout time.Duration) *TerminateSignal {
	return NewTerminateSignalWithContext(context.Background(), timeout)
}

func NewDefaultTerminateSignal() *TerminateSignal {
	return NewTerminateSignalWithContext(context.Background(), ConstInfinityTerminateTimeout)
}

func (s *TerminateSignal) CancelCallbacksRegistry(callbacks ...func()) {
	s.callbacks = append(s.callbacks, callbacks...)
}

func (s *TerminateSignal) GetStopChan() <-chan struct{} {
	return s.ctx.Done()
}

func (s *TerminateSignal) Close(outsideWg *sync.WaitGroup) {
	s.closeOnce.Do(func() {
		// 启动通知停止工作
		for _, cb := range s.callbacks {
			if cb != nil {
				s.wg.Add(1)
				go s.executeCallback(cb)
			}
		}
		// 退出动作
		s.cancel()
		// 等待结束
		s.wg.Wait()
		// 关闭外部信号
		if outsideWg != nil {
			outsideWg.Done()
		}
	})
}

func (s *TerminateSignal) executeCallback(callback func()) {
	defer s.wg.Done()
	<-s.ctx.Done()
	callback()
}
