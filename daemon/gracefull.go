package signal

import (
	"os"
	"os/signal"
	"sync"
	"syscall"
)

func WaitTermSignalForGracefulShutdown(sigs ...*TerminateSignal) {
	// 捕获系统信号量
	quit := make(chan os.Signal, 1)
	// 参考资料
	//  https://blog.wu-boy.com/2021/03/why-use-buffered-channel-in-signal-notify/
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	//  退出
	<-quit
	signal.Stop(quit)
	close(quit)
	// 相应多个关闭信号
	if len(sigs) > 0 {
		wg := sync.WaitGroup{}
		wg.Add(len(sigs))
		for _, s := range sigs {
			go s.Close(&wg)
		}
		wg.Wait()
	}
}
