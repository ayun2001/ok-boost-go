package client

import (
	"context"
	"database/sql"
	"fmt"
	"net"
	"sync"
	"time"

	boost "gitee.com/ayun2001/ok-boost-go"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"go.uber.org/zap"
	driver "gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

var (
	defaultCharsets = []string{ConstMysqlUtf8Charset, ConstMysqlUtf8mb4Charset}
)

type Config struct {
	Address            string             `json:"address" yaml:"address"`                                           // 数据库服务器地址
	Port               uint16             `json:"port" yaml:"port"`                                                 // 端口号
	ReadTimeout        uint32             `json:"readTimeout,omitempty" yaml:"readTimeout,omitempty"`               // 读取超时时间，单位为毫秒
	WriteTimeout       uint32             `json:"writeTimeout,omitempty" yaml:"writeTimeout,omitempty"`             // 写入超时时间，单位为毫秒
	Username           string             `json:"username" yaml:"username"`                                         // 登录数据库的用户名
	Password           string             `json:"password" yaml:"password"`                                         // 登录数据库的密码
	DB                 string             `json:"db" yaml:"db"`                                                     // 要连接的数据库名称
	Charset            string             `json:"charset" yaml:"charset"`                                           // 连接数据库使用的字符集
	MaxIdle            int                `json:"maxIdle,omitempty" yaml:"maxIdle,omitempty"`                       // 最大空闲连接数，如果设置为0，则将禁用连接池，即每次访问都会打开新的连接。
	MaxOpen            int                `json:"maxOpen,omitempty" yaml:"maxOpen,omitempty"`                       // 最大打开连接数
	SlowQueryThreshold uint32             `json:"slowQueryThreshold,omitempty" yaml:"slowQueryThreshold,omitempty"` // 慢查询阈值，单位为毫秒。如果查询的执行时间超过该值，则被视为慢查询。
	ExecBatchSize      uint16             `json:"execBatchSize,omitempty" yaml:"execBatchSize,omitempty"`           // 批处理执行的大小，表示在每个SQL语句中可以提交的最大参数数量。
	Logger             *zap.SugaredLogger `json:"-" yaml:"-"`
}

type FeatureOpts struct {
	EnableMetrics bool `json:"enableMetrics,omitempty" yaml:"enableMetrics,omitempty"` // 开启 metrics 记录
}

type Pool struct {
	clientMetrics *clientMetrics
	clientConf    *Config
	featureOpts   *FeatureOpts
	closeOnce     sync.Once
	stopCtx       context.Context
	stopCancel    context.CancelFunc
	wg            sync.WaitGroup
	instance      *gorm.DB
	sqlDB         *sql.DB
}

func NewDefaultFeatureOpts() *FeatureOpts {
	return &FeatureOpts{
		EnableMetrics: true,
	}
}

func NewMysqlClientPool(clientConf *Config, featureOpts *FeatureOpts) *Pool {
	clientConf = validMySqlClientConfig(clientConf)
	featureOpts = validFeatureOpts(featureOpts)

	var p = Pool{
		clientMetrics: &clientMetrics{},
		clientConf:    clientConf,
		featureOpts:   featureOpts,
		closeOnce:     sync.Once{},
		wg:            sync.WaitGroup{},
	}

	p.stopCtx, p.stopCancel = context.WithCancel(context.Background())
	p.clientMetrics = newMysqlClientMetrics()

	if p.featureOpts.EnableMetrics {
		p.clientMetrics.registerMetrics()
	}

	// 创建数据库
	if !p.createDb() {
		p.Stop()
		return nil
	}

	// 连接数据库
	if !p.connect() {
		p.Stop()
		return nil
	}

	// 启动看门人
	p.wg.Add(1)
	go p.probeKeeper()

	if p.featureOpts.EnableMetrics {
		// 启动状态同步器
		p.wg.Add(1)
		go p.statusSyncer()
	}

	return &p
}

func (p *Pool) Stop() {
	p.closeOnce.Do(func() {
		p.stopCancel()
		// 等待关闭守护
		p.wg.Wait()
		// 关闭连接池
		if p.instance != nil && p.sqlDB != nil {
			err := p.sqlDB.Close()
			if err != nil {
				boost.Logger.Warnw("failed to shutdown mysql client pool", "error", err)
			}
			boost.Logger.Infow("mysql client pool is shutdown")
		}
		// 关闭 metrics
		if p.featureOpts.EnableMetrics {
			p.clientMetrics.unRegisterMetrics()
		}
		// 释放资源
		p.sqlDB = nil
		p.instance = nil
	})
}

func (p *Pool) GetSession(val interface{}) *gorm.DB {
	return p.instance.Model(val)
}

func (p *Pool) GetUnSafeClient() *gorm.DB {
	return p.instance
}

func (p *Pool) statusSyncer() {
	ticker := time.NewTicker(time.Millisecond * ConstHealthCheckInterval)

	defer func() {
		ticker.Stop()
		p.wg.Done()
	}()

	for {
		select {
		case <-ticker.C:
			// 执行状态同步
			p.status()
			break
		case <-p.stopCtx.Done(): // 如果 stopCtx 被取消，结束循环
			return
		}
	}
}

func (p *Pool) status() {
	port := utils.Uint64ToString(uint64(p.clientConf.Port))
	app := utils.ApplicationName

	s := p.sqlDB.Stats()
	p.clientMetrics.openSessions.WithLabelValues(p.clientConf.Address, port, p.clientConf.DB, app).Set(float64(s.OpenConnections))
	p.clientMetrics.inUseSessions.WithLabelValues(p.clientConf.Address, port, p.clientConf.DB, app).Set(float64(s.InUse))
	p.clientMetrics.idleSessions.WithLabelValues(p.clientConf.Address, port, p.clientConf.DB, app).Set(float64(s.Idle))
	p.clientMetrics.waitingSessions.WithLabelValues(p.clientConf.Address, port, p.clientConf.DB, app).Set(float64(s.WaitCount))
}

func (p *Pool) probeKeeper() {
	ticker := time.NewTicker(time.Millisecond * ConstHealthCheckInterval)

	defer func() {
		ticker.Stop()
		p.wg.Done()
	}()

	for {
		select {
		case <-ticker.C:
			// 执行探测
			if err := p.probe(); err != nil {
				boost.Logger.Errorw("health check probeKeeper failed", "error", err)
			}
			break
		case <-p.stopCtx.Done(): // 如果 stopCtx 被取消，结束循环
			return
		}
	}
}

func (p *Pool) probe() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond*ConstProbeContextTimeout)
	defer cancel()
	return p.sqlDB.PingContext(ctx)
}

func (p *Pool) createDb() bool {
	var (
		err  error
		conn *sql.DB
	)

	// 创建连接实例, 只有连接 MySQL 根环境，才能自动创建数据
	dbUrl := fmt.Sprintf("%s:%s@tcp(%s:%d)/?charset=%s&parseTime=True&loc=Local",
		p.clientConf.Username, p.clientConf.Password, p.clientConf.Address, p.clientConf.Port, p.clientConf.Charset)

	// 先创建 mysql 连接，判断数据库是否存在，如果不存在然后创建数据库
	conn, err = sql.Open("mysql", dbUrl)
	if err != nil {
		boost.Logger.Fatalw("create mysql init connection failed", "error", err)
		return false
	}

	// 关闭初始化连接
	defer closeSqlConnect(conn)
	// 创建对应的数据库
	_, err = conn.Exec(fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s;", p.clientConf.DB))
	if err != nil {
		boost.Logger.Fatalw(fmt.Sprintf("create database %s failed", p.clientConf.DB), "error", err)
		return false
	}

	// 返回
	boost.Logger.Infof("create db: %s success if not exists", p.clientConf.DB)
	return true
}

func (p *Pool) connect() bool {
	var err error

	// 创建 gorm 数据库连接
	dbUrl := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s&parseTime=True&loc=Local",
		p.clientConf.Username, p.clientConf.Password, p.clientConf.Address, p.clientConf.Port, p.clientConf.DB, p.clientConf.Charset)
	logger := newGormLogger(p.clientConf.Logger, zap.InfoLevel, zap.InfoLevel, p.clientConf.SlowQueryThreshold)

	p.instance, err = gorm.Open(driver.Open(dbUrl), &gorm.Config{
		Logger:                 logger,                          // 设置日志为 zap 日志格式
		CreateBatchSize:        int(p.clientConf.ExecBatchSize), // 创建& 关联 INSERT 一次批量执行的数量
		SkipDefaultTransaction: false,                           // 对于写操作（创建、更新、删除），为了确保数据的完整性，GORM 会将它们封装在事务内运行
		DryRun:                 false,                           // 关闭生成 SQL 但不执行的功能
		NamingStrategy: schema.NamingStrategy{
			TablePrefix:   "t_", // 表名前缀
			SingularTable: true, // 使用单数表名，启用该选项后，`User` 表将是`user`
		},
	})
	if err != nil {
		boost.Logger.Fatalw("failed to connect to mysql server", "error", err)
		return false
	}
	// 获得 Mysql 连接池实例
	p.sqlDB, err = p.instance.DB()
	if err != nil {
		boost.Logger.Fatalw("failed to get one mysql connection from the pool", "error", err)
		return false
	}
	// 设置连接池参数
	p.sqlDB.SetMaxIdleConns(p.clientConf.MaxIdle)
	p.sqlDB.SetMaxOpenConns(p.clientConf.MaxOpen)
	p.sqlDB.SetConnMaxLifetime(15 * time.Minute) // 连接连续复用窗口时间。太长，mysql 可能有问题，太短，效率差

	// 连接测试下
	err = p.probe()
	if err != nil {
		boost.Logger.Fatalw("failed to connect to mysql server", "error", err)
		return false
	}

	// 返回
	boost.Logger.Infof("connect mysql %s:%d#%s success", p.clientConf.Address, p.clientConf.Port, p.clientConf.DB)
	return true
}

func closeSqlConnect(c *sql.DB) {
	if err := c.Close(); err != nil {
		boost.Logger.Errorw("failed to close the connection", "error", err)
	}
}

func checkCharset(charset string) bool {
	for _, c := range defaultCharsets {
		if c == charset {
			return true
		}
	}
	return false
}

func validMySqlClientConfig(conf *Config) *Config {
	if conf != nil {
		// 判断地址
		if ip := net.ParseIP(conf.Address); ip == nil {
			conf.Address = "127.0.0.1"
		}
		// 判断端口
		if conf.Port <= 0 {
			conf.Port = 3306
		}
		// 判断DB名称
		if len(conf.DB) == 0 {
			conf.DB = ConstMysqlDefaultDbName
		}
		// 确保字符集一定是 utf8 和 utf8mb4
		if !checkCharset(conf.Charset) {
			conf.Charset = ConstMysqlUtf8mb4Charset
		}
		if conf.MaxOpen <= 0 {
			conf.MaxOpen = 10
		}
		if conf.MaxIdle <= 0 {
			conf.MaxIdle = 3
		}
		// 确保慢查询时长有效
		if conf.SlowQueryThreshold <= 0 {
			conf.SlowQueryThreshold = ConstSlowQueryTimeThreshold
		}
		// 确保批量执行数据有效
		if conf.ExecBatchSize <= 0 {
			conf.ExecBatchSize = ConstExecBatchDefaultSize
		}
		if conf.Logger == nil {
			conf.Logger = boost.Logger
		}
	} else {
		conf = &Config{
			Address:            "127.0.0.1",
			Port:               3306,
			ReadTimeout:        ConstDatabaseReadTimeout,
			WriteTimeout:       ConstDatabaseWriteTimeout,
			Username:           "test",
			Password:           "123456",
			DB:                 ConstMysqlDefaultDbName,
			Charset:            ConstMysqlUtf8mb4Charset,
			MaxIdle:            3,
			MaxOpen:            10,
			SlowQueryThreshold: ConstSlowQueryTimeThreshold,
			ExecBatchSize:      ConstExecBatchDefaultSize,
		}
	}

	return conf
}

func validFeatureOpts(opts *FeatureOpts) *FeatureOpts {
	if opts != nil {
		return opts
	}

	return NewDefaultFeatureOpts()
}
