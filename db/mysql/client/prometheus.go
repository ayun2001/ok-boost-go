package client

import (
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/prometheus/client_golang/prometheus"
)

type clientMetrics struct {
	openSessions    *prometheus.GaugeVec
	inUseSessions   *prometheus.GaugeVec
	idleSessions    *prometheus.GaugeVec
	waitingSessions *prometheus.GaugeVec
}

func newMysqlClientMetrics() *clientMetrics {
	return &clientMetrics{}
}

func (m *clientMetrics) registerMetrics() {
	var metricLabels = []string{"host", "port", "db", "app"}
	var id = utils.GetRandIdString()

	m.openSessions = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "mysql_client_total_sessions_count",
			Help:        "The total number of established connections both in use and idle",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	m.inUseSessions = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "mysql_client_inuse_sessions_count",
			Help:        "The total number of connections currently in use",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	m.idleSessions = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "mysql_client_idle_sessions_count",
			Help:        "The total number of idle connections",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	m.waitingSessions = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "mysql_client_waiting_sessions_count",
			Help:        "The total number of connections waited for",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	prometheus.MustRegister(m.openSessions, m.inUseSessions, m.idleSessions, m.waitingSessions)
}

func (m *clientMetrics) unRegisterMetrics() {
	prometheus.Unregister(m.openSessions)
	prometheus.Unregister(m.inUseSessions)
	prometheus.Unregister(m.idleSessions)
	prometheus.Unregister(m.waitingSessions)
}
