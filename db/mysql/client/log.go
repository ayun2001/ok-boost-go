package client

import (
	"context"
	"strings"
	"time"

	boost "gitee.com/ayun2001/ok-boost-go"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	gormNativeLogger "gorm.io/gorm/logger"
)

// gormLogger 使用 zap 来打印 gorm 的日志
// 初始化时在内部的 logger 中添加 trace id 可以追踪 sql 执行记录
type gormLogger struct {
	logger *zap.SugaredLogger
	// 日志级别
	logLevel zapcore.Level
	// 指定慢查询时间记录阀值
	slowQueryThreshold time.Duration
	// Trace 方法打印日志是使用的日志 level
	traceLogWithLevel zapcore.Level
}

var gormLogLevelMap = map[gormNativeLogger.LogLevel]zapcore.Level{
	gormNativeLogger.Info:  zap.InfoLevel,
	gormNativeLogger.Warn:  zap.WarnLevel,
	gormNativeLogger.Error: zap.ErrorLevel,
}

func (g *gormLogger) Info(_ context.Context, msg string, data ...interface{}) {
	if g.logLevel <= zap.InfoLevel {
		g.logger.Infof(msg, data...)
	}
}

func (g *gormLogger) Warn(_ context.Context, msg string, data ...interface{}) {
	if g.logLevel <= zap.WarnLevel {
		g.logger.Warnf(msg, data...)
	}
}

func (g *gormLogger) Error(_ context.Context, msg string, data ...interface{}) {
	if g.logLevel <= zap.ErrorLevel {
		g.logger.Errorf(msg, data...)
	}
}

func (g *gormLogger) Trace(_ context.Context, begin time.Time, fc func() (string, int64), err error) {
	latency := utils.TruncateDecimal4(float64(time.Since(begin).Milliseconds()))
	sql, rows := fc()
	sql = removeDuplicateWhitespace(sql, true)

	if err != nil {
		g.logger.Errorw("failed to exec sql", "error", err.Error(), "sql", sql, "latency(ms)", latency, "rows", rows)
		return
	}

	if g.slowQueryThreshold > 0 && latency > float64(g.slowQueryThreshold.Milliseconds()) {
		g.logger.Warnw("slow query when time threshold exceeded", "sql", sql, "latency(ms)", latency, "threshold(ms)", g.slowQueryThreshold.Milliseconds(), "rows", rows)
		return
	}

	switch {
	case g.traceLogWithLevel <= zap.WarnLevel:
		g.logger.Warnw("trace log warning level", "sql", sql, "latency(ms)", utils.TruncateDecimal4(latency*1000), "rows", rows)
	case g.traceLogWithLevel <= zap.ErrorLevel:
		g.logger.Errorw("trace log error level", "sql", sql, "latency(ms)", utils.TruncateDecimal4(latency*1000), "rows", rows)
	case g.traceLogWithLevel <= zap.InfoLevel:
		if !boost.IsReleaseMode() {
			g.logger.Info("trace log info level", "sql", sql, "latency(ms)", utils.TruncateDecimal4(latency*1000), "rows", rows)
		}
	}
}

// LogMode 实现 gorm logger 接口方法
func (g *gormLogger) LogMode(gormLogLevel gormNativeLogger.LogLevel) gormNativeLogger.Interface {
	zapLevel, exists := gormLogLevelMap[gormLogLevel]
	if !exists {
		zapLevel = zap.DebugLevel
	}
	newLogger := *g
	newLogger.logLevel = zapLevel
	return &newLogger
}

func newGormLogger(log *zap.SugaredLogger, logLevel, traceLogWithLevel zapcore.Level, slowQueryTimeThreshold uint32) *gormLogger {
	return &gormLogger{
		logger:             log,
		logLevel:           logLevel,
		slowQueryThreshold: time.Duration(slowQueryTimeThreshold) * time.Millisecond,
		traceLogWithLevel:  traceLogWithLevel,
	}
}

func removeDuplicateWhitespace(s string, trim bool) string {
	s = strings.Join(strings.Fields(s), " ")
	if trim {
		return strings.TrimSpace(s)
	} else {
		return s
	}
}
