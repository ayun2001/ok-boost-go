package client

const (
	ConstMysqlUtf8Charset       = "utf8"
	ConstMysqlUtf8mb4Charset    = "utf8mb4"
	ConstDatabaseReadTimeout    = 10000
	ConstDatabaseWriteTimeout   = 10000
	ConstHealthCheckInterval    = 5000
	ConstProbeContextTimeout    = 5000
	ConstSlowQueryTimeThreshold = 2000 // 毫秒
	ConstExecBatchDefaultSize   = 50   // 一次执行批量任务最大多少
	ConstMysqlDefaultDbName     = "test_db"
)
