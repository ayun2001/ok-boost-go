package client

import (
	"time"

	"gorm.io/gorm"
	"gorm.io/plugin/soft_delete"
)

type BaseTimeField struct {
	CreatedAt *time.Time `json:"-" yaml:"-" swaggerignore:"true"`
	UpdatedAt *time.Time `json:"-" yaml:"-" swaggerignore:"true"`
}

type BaseTimeFieldWithSoftDelete struct {
	BaseTimeField
	DeleteAt  gorm.DeletedAt        `json:"-" yaml:"-" swaggerignore:"true"`
	IsDeleted soft_delete.DeletedAt `json:"-" yaml:"-" gorm:"softDelete:flag" swaggerignore:"true"`
}

type BaseModel struct {
	ID int64 `gorm:"primaryKey" json:"id" yaml:"id" swaggerignore:"true"`
	BaseTimeField
}

type BaseModelWithSoftDelete struct {
	ID int64 `gorm:"primaryKey" json:"id" yaml:"id" swaggerignore:"true"`
	BaseTimeFieldWithSoftDelete
}
