package client

import (
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/prometheus/client_golang/prometheus"
)

type clientMetrics struct {
	idleSessionsNumber  *prometheus.GaugeVec
	totalSessionsNumber *prometheus.GaugeVec
	staleSessionsNumber *prometheus.GaugeVec
	poolHitsNumber      *prometheus.GaugeVec
	poolMissesNumber    *prometheus.GaugeVec
}

func newRedisClientMetrics() *clientMetrics {
	return &clientMetrics{}
}

func (m *clientMetrics) registerMetrics() {
	var metricLabels = []string{"host", "port", "db", "app"}
	var id = utils.GetRandIdString()

	m.totalSessionsNumber = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "redis_client_total_sessions_count",
			Help:        "The total number of total connections in the pool",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	m.idleSessionsNumber = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "redis_client_idle_sessions_count",
			Help:        "The total number of idle connections in the pool",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	m.staleSessionsNumber = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "redis_client_stale_sessions_count",
			Help:        "The total number of stale connections removed from the pool",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	m.poolHitsNumber = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "redis_client_free_session_hits_times",
			Help:        "The total number of times free connection was found in the pool",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	m.poolMissesNumber = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace:   utils.ConstDefaultPrometheusNamespace,
			Subsystem:   utils.ConstDefaultPrometheusSubsystemName,
			Name:        "redis_client_free_session_misses_times",
			Help:        "The total number of times free connection was NOT found in the pool",
			ConstLabels: map[string]string{"id": id},
		}, metricLabels,
	)

	prometheus.MustRegister(m.totalSessionsNumber, m.idleSessionsNumber, m.staleSessionsNumber, m.poolHitsNumber, m.poolMissesNumber)
}

func (m *clientMetrics) unRegisterMetrics() {
	prometheus.Unregister(m.totalSessionsNumber)
	prometheus.Unregister(m.idleSessionsNumber)
	prometheus.Unregister(m.staleSessionsNumber)
	prometheus.Unregister(m.poolHitsNumber)
	prometheus.Unregister(m.poolMissesNumber)
}
