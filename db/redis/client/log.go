package client

import (
	"context"

	boost "gitee.com/ayun2001/ok-boost-go"
)

type redisLogger struct{}

func (r *redisLogger) Printf(_ context.Context, msg string, data ...interface{}) {
	boost.Logger.Infof(msg, data...)
}

func newRedisZapLogger() *redisLogger {
	return &redisLogger{}
}
