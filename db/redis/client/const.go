package client

const (
	ConstDatabaseConnectTimeout = 10000
	ConstHealthCheckInterval    = 5000
	ConstDatabaseReadTimeout    = 10000
	ConstDatabaseWriteTimeout   = 10000
	ConstProbeContextTimeout    = 5000
)
