package client

import (
	"context"
	"fmt"
	"net"
	"sync"
	"time"

	boost "gitee.com/ayun2001/ok-boost-go"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/go-redis/redis/v8"
)

type Config struct {
	Address      string `json:"address" yaml:"address"`                               // 连接地址
	Port         uint16 `json:"port" yaml:"port"`                                     // 端口号
	ReadTimeout  uint32 `json:"readTimeout,omitempty" yaml:"readTimeout,omitempty"`   // 读超时时间，单位毫秒
	WriteTimeout uint32 `json:"writeTimeout,omitempty" yaml:"writeTimeout,omitempty"` // 写超时时间，单位毫秒
	Password     string `json:"password" yaml:"password"`                             // 密码
	DB           uint8  `json:"db" yaml:"db"`                                         // DB的名称或编号
	MinIdle      int    `json:"minIdle,omitempty" yaml:"minIdle,omitempty"`           // 最小空闲连接数（针对于连接池）
	MaxOpen      int    `json:"maxOpen,omitempty" yaml:"maxOpen,omitempty"`           // 最大打开连接数（针对于连接池）
}

type FeatureOpts struct {
	EnableMetrics bool `json:"enableMetrics,omitempty" yaml:"enableMetrics,omitempty"` // 开启 metrics 记录
}

type Pool struct {
	clientMetrics   *clientMetrics
	clientConf      *Config
	featureOpts     *FeatureOpts
	closeOnce       sync.Once
	stopCtx         context.Context
	stopCancel      context.CancelFunc
	wg              sync.WaitGroup
	instance        *redis.Client
	instanceSession *Session
}

func NewDefaultFeatureOpts() *FeatureOpts {
	return &FeatureOpts{
		EnableMetrics: true,
	}
}

type Session struct {
	*redis.Client
}

func (s *Session) Close() error {
	return nil
}

func init() {
	// 设置日志
	redis.SetLogger(newRedisZapLogger())
}

func NewRedisClient(clientConf *Config, featureOpts *FeatureOpts) *Pool {
	clientConf = validRedisClientConfig(clientConf)
	featureOpts = validFeatureOpts(featureOpts)

	var p = Pool{
		clientConf:  clientConf,
		featureOpts: featureOpts,
		closeOnce:   sync.Once{},
		wg:          sync.WaitGroup{},
	}

	p.stopCtx, p.stopCancel = context.WithCancel(context.Background())
	p.clientMetrics = newRedisClientMetrics()

	if p.featureOpts.EnableMetrics {
		p.clientMetrics.registerMetrics()
	}

	// 连接数据库
	if !p.connect() {
		p.Stop()
		return nil
	}

	// 启动看门人
	p.wg.Add(1)
	go p.probeKeeper()

	if p.featureOpts.EnableMetrics {
		// 启动状态同步器
		p.wg.Add(1)
		go p.statusSyncer()
	}

	return &p
}

func (p *Pool) Stop() {
	p.closeOnce.Do(func() {
		p.stopCancel()
		// 等待关闭守护
		p.wg.Wait()
		// 关闭连接池
		if p.instance != nil {
			err := p.instance.Close()
			if err != nil {
				boost.Logger.Warnw("failed to shutdown redis client pool", "error", err)
			}
			boost.Logger.Infow("redis client pool is shutdown")
		}
		// 关闭 metrics
		if p.featureOpts.EnableMetrics {
			p.clientMetrics.unRegisterMetrics()
		}
		// 指针修复
		p.instanceSession = nil
		p.instance = nil
	})
}

func (p *Pool) GetSession() *Session {
	return p.instanceSession
}

func (p *Pool) GetUnSafeClient() *redis.Client {
	return p.instance
}

func (p *Pool) statusSyncer() {
	boost.Logger.Infow("status syncer is ready")
	ticker := time.NewTicker(time.Millisecond * ConstHealthCheckInterval)

	defer func() {
		ticker.Stop()
		p.wg.Done()
		boost.Logger.Infow("status syncer is shutdown")
	}()

	for {
		select {
		case <-ticker.C:
			// 执行状态同步
			p.status()
			break
		case <-p.stopCtx.Done(): // 如果 stopCtx 被取消，结束循环
			return
		}
	}
}

func (p *Pool) status() {
	port := utils.Uint64ToString(uint64(p.clientConf.Port))
	db := utils.Uint64ToString(uint64(p.clientConf.DB))
	app := utils.ApplicationName

	s := p.instance.PoolStats()
	p.clientMetrics.totalSessionsNumber.WithLabelValues(p.clientConf.Address, port, db, app).Set(float64(s.TotalConns))
	p.clientMetrics.idleSessionsNumber.WithLabelValues(p.clientConf.Address, port, db, app).Set(float64(s.IdleConns))
	p.clientMetrics.staleSessionsNumber.WithLabelValues(p.clientConf.Address, port, db, app).Set(float64(s.StaleConns))
	p.clientMetrics.poolHitsNumber.WithLabelValues(p.clientConf.Address, port, db, app).Set(float64(s.Hits))
	p.clientMetrics.poolMissesNumber.WithLabelValues(p.clientConf.Address, port, db, app).Set(float64(s.Misses))
}

func (p *Pool) probeKeeper() {
	ticker := time.NewTicker(time.Millisecond * ConstHealthCheckInterval)

	defer func() {
		ticker.Stop()
		p.wg.Done()
	}()

	for {
		select {
		case <-ticker.C:
			// 执行探测
			if err := p.probe(); err != nil {
				boost.Logger.Errorw("health check probeKeeper failed", "error", err)
			}
			break
		case <-p.stopCtx.Done(): // 如果 stopCtx 被取消，结束循环
			return
		}
	}
}

func (p *Pool) probe() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond*ConstProbeContextTimeout)
	defer cancel()
	return p.instance.Ping(ctx).Err()
}

func (p *Pool) connect() bool {
	var err error

	p.instance = redis.NewClient(&redis.Options{
		Addr:         fmt.Sprintf("%s:%d", p.clientConf.Address, p.clientConf.Port),
		Password:     p.clientConf.Password,
		DB:           int(p.clientConf.DB),
		PoolSize:     p.clientConf.MaxOpen,
		MinIdleConns: p.clientConf.MinIdle,
		ReadTimeout:  time.Duration(p.clientConf.ReadTimeout) * time.Millisecond,
		WriteTimeout: time.Duration(p.clientConf.WriteTimeout) * time.Millisecond,
	})

	// 连接测试下
	err = p.probe()
	if err != nil {
		boost.Logger.Fatalw("failed to connect to redis server", "error", err)
		return false
	}

	// 创建会话对象
	p.instanceSession.Client = p.instance

	// 返回
	boost.Logger.Infof("connect redis %s:%d#%s success", p.clientConf.Address, p.clientConf.Port, p.clientConf.DB)
	return true
}

func validRedisClientConfig(conf *Config) *Config {
	if conf != nil {
		// 判断地址
		if ip := net.ParseIP(conf.Address); ip == nil {
			conf.Address = "127.0.0.1"
		}
		// 判断端口
		if conf.Port <= 0 {
			conf.Port = 6379
		}
		// 判断DB名称
		if conf.DB >= 15 {
			conf.DB = 0
		}
		if conf.MaxOpen <= 0 {
			conf.MaxOpen = 10
		}
		if conf.MinIdle <= 0 {
			conf.MinIdle = 3
		}
	} else {
		conf = &Config{
			Address:      "127.0.0.1",
			Port:         6379,
			ReadTimeout:  ConstDatabaseReadTimeout,
			WriteTimeout: ConstDatabaseWriteTimeout,
			Password:     "",
			DB:           0,
			MinIdle:      3,
			MaxOpen:      10,
		}
	}

	return conf
}

func validFeatureOpts(opts *FeatureOpts) *FeatureOpts {
	if opts != nil {
		return opts
	}

	return NewDefaultFeatureOpts()
}
