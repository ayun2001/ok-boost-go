package httptool

import "github.com/gin-gonic/gin/binding"

const (
	ConstHttpHeaderContentType                = "Content-Type"
	ConstHttpHeaderJsonContentTypeValue       = binding.MIMEJSON
	ConstHttpHeaderXmlContentTypeValue        = binding.MIMEXML
	ConstHttpHeaderXml2ContentTypeValue       = binding.MIMEXML2
	ConstHttpHeaderYamlContentTypeValue       = binding.MIMEYAML
	ConstHttpHeaderTomlContentTypeValue       = binding.MIMETOML
	ConstHttpHeaderTextContentTypeValue       = binding.MIMEPlain
	ConstHttpHeaderJavascriptContentTypeValue = "application/javascript"
)
