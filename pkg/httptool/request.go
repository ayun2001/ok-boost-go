package httptool

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

var contentTypes = []string{
	ConstHttpHeaderJsonContentTypeValue,
	ConstHttpHeaderJavascriptContentTypeValue,
	ConstHttpHeaderTextContentTypeValue,
	ConstHttpHeaderXmlContentTypeValue,
	ConstHttpHeaderXml2ContentTypeValue,
	ConstHttpHeaderYamlContentTypeValue,
	ConstHttpHeaderTomlContentTypeValue,
}

// CalcRequestSize返回请求(request)对象的大小
func CalcRequestSize(r *http.Request) int64 {
	size := 0

	// 计算URL的字符串长度
	if r.URL != nil {
		size += len(r.URL.String())
	}

	// 将方法(Method)和协议(Proto)放到size变量中
	size += len(r.Method)
	size += len(r.Proto)

	// 遍历 header，统计 header 的键值对大小，并加到请求大小中
	for name, values := range r.Header {
		size += len(name)
		for _, value := range values {
			size += len(value)
		}
	}

	// size中增加了主机名(Host)的大小
	size += len(r.Host)

	// 如果ContentLength设置不为-1，则将ContentLength添加到size中
	if r.ContentLength != -1 {
		size += int(r.ContentLength)
	}

	return int64(size)
}

// StringFilterFlags 返回给定字符串中第一个标记。
func StringFilterFlags(content string) string {
	// 将字符串中第一个 ';' 或 ' ' 前的所有字符作为第一个标记。如果两者都不存在，则返回整个字符串。
	if i := strings.IndexAny(content, "; "); i >= 0 {
		return content[:i]
	}
	return content
}

// CanRecordContextBody 检查HTTP请求头中是否存在特定内容类型的值。
func CanRecordContextBody(h http.Header) bool {
	v := h.Get(ConstHttpHeaderContentType)

	// 如果请求头为空或者内容信息不足以区分类型，则直接返回false
	if v == "" || !strings.Contains(v, "/") {
		return false
	}

	// 查找所有 definedContentTypes 列表中指定的内容类型。
	typeStr := StringFilterFlags(v)
	for _, ct := range contentTypes {
		if strings.HasPrefix(typeStr, ct) {
			return true
		}
	}

	// 如果内容类型未被定义则返回false
	return false
}

// HttpHandlerToGinHandlerFunc 将标准的 http.Handler 包装为 gin.HandlerFunc
func HttpHandlerToGinHandlerFunc(handler http.Handler) gin.HandlerFunc {
	// 返回一个匿名函数作为 gin.HandlerFunc
	return func(c *gin.Context) {
		// 调用输入的 http.Handler 的 ServeHTTP 方法
		// 并将 gin.Context.Writer 和 gin.Context.Request 传递作为参数
		handler.ServeHTTP(c.Writer, c.Request)
	}
}

// HttpHandlerFuncToGinHandlerFunc 将标准的 http.HandlerFunc 包装为 gin.HandlerFunc
func HttpHandlerFuncToGinHandlerFunc(handler http.HandlerFunc) gin.HandlerFunc {
	// 返回一个匿名函数作为 gin.HandlerFunc
	return func(c *gin.Context) {
		// 调用输入的 http.Handler 的 ServeHTTP 方法
		// 并将 gin.Context.Writer 和 gin.Context.Request 传递作为参数
		handler.ServeHTTP(c.Writer, c.Request)
	}
}

func ExtContextTypeToUTF8(s string) string {
	switch s {
	case ConstHttpHeaderJsonContentTypeValue:
		return ConstHttpHeaderJsonContentTypeValue + "; charset=utf-8"
	case ConstHttpHeaderJavascriptContentTypeValue:
		return ConstHttpHeaderJavascriptContentTypeValue + "; charset=utf-8"
	case ConstHttpHeaderXmlContentTypeValue:
		return ConstHttpHeaderXmlContentTypeValue + "; charset=utf-8"
	case ConstHttpHeaderXml2ContentTypeValue:
		return ConstHttpHeaderXml2ContentTypeValue + "; charset=utf-8"
	case ConstHttpHeaderYamlContentTypeValue:
		return ConstHttpHeaderYamlContentTypeValue + "; charset=utf-8"
	case ConstHttpHeaderTomlContentTypeValue:
		return ConstHttpHeaderTomlContentTypeValue + "; charset=utf-8"
	case ConstHttpHeaderTextContentTypeValue:
		return ConstHttpHeaderTextContentTypeValue + "; charset=utf-8"
	default:
		return s
	}
}
