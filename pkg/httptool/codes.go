package httptool

const (
	HttpRequestOkCode    int64 = 0
	HttpRequestErrorCode int64 = iota + 10
)

const (
	HttpRequestOk = "success"
)
