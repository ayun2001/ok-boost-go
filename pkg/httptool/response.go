package httptool

type HttpResponseItemsTotal struct {
	TotalCount int64 `json:"totalCount" yaml:"totalCount"` // 响应对象总数
}

type HttpResponseItemsID struct {
	ID int64 `json:"id" yaml:"id"` // 响应对象 Id
}

type HttpQueryPaginated struct {
	PageIndex uint32 `json:"pageIndex" yaml:"pageIndex"` // 当前页数量
	PageSize  uint32 `json:"pageSize" yaml:"pageSize"`   // 每页显示最大条目数
	Desc      bool   `json:"desc" yaml:"desc"`           // 是否倒序
}

type BaseHttpResponse struct {
	Code         int64       `json:"errorCode" yaml:"errorCode"`                           // 响应代码
	ErrorMessage string      `json:"errorMessage,omitempty" yaml:"errorMessage,omitempty"` // 错误信息
	ErrorDetail  interface{} `json:"errorDetail,omitempty" yaml:"errorDetail,omitempty"`   // 错误详细信息
	Data         interface{} `json:"data,omitempty" yaml:"data,omitempty"`                 // 响应数据
}

// HttpResponsePaginated 匿名对象后面不需要 json 相关的描述，就回平铺结构
type HttpResponsePaginated struct {
	HttpResponseItemsTotal
	HttpQueryPaginated
	BaseHttpResponse
}
