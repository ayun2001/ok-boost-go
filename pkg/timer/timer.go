package timer

import (
	"context"
	"sync"
	"sync/atomic"
	"time"
)

// CachedTimer 存储每秒 Unix 时间戳，并返回缓存的值
type CachedTimer struct {
	now        atomic.Int64 // 当前时间戳
	closeOnce  sync.Once    // 保证计时器只关闭一次
	stopCtx    context.Context
	stopCancel context.CancelFunc
	wg         sync.WaitGroup
}

// 辅助函数，返回秒级 Unix 时间戳
func getUnixTime() int64 {
	return time.Now().Unix()
}

// 创建和启动 CachedTimer 计时器，每秒更新一次时间戳。
func NewCachedTimer() *CachedTimer {
	tr := &CachedTimer{
		now:       atomic.Int64{},
		wg:        sync.WaitGroup{},
		closeOnce: sync.Once{},
	}

	tr.now.Store(getUnixTime())
	tr.stopCtx, tr.stopCancel = context.WithCancel(context.Background())

	tr.wg.Add(1)
	go tr.update() // 启动时间戳更新协程

	return tr
}

// 返回当前 Unix 时间戳值
func (t *CachedTimer) Now() int64 {
	return t.now.Load()
}

// 停止计时器，结束时间戳更新协程。
func (t *CachedTimer) Stop() {
	t.closeOnce.Do(func() {
		t.stopCancel()
		t.wg.Wait()
	})
}

// 定期检查和更新时间戳
func (t *CachedTimer) update() {
	ticker := time.NewTicker(time.Second)

	defer func() {
		ticker.Stop()
		t.wg.Done()
	}()

	for {
		select {
		case <-t.stopCtx.Done(): // 如果 stopCtx 被取消，结束循环
			return
		case <-ticker.C: // 每秒走一次这个分支，更新时间戳的值
			t.now.Store(getUnixTime()) // 将新的时间戳存储在原子计数器中，以免竞争情况发生。
		}
	}
}
