package utils

import (
	"sort"
)

// []string 内元素 Remove Duplicate
func RemoveDuplicatesAndEmptyS(o []string) (ret []string) {
	// 排序
	e := make([]string, len(o))
	copy(e, o)
	sort.Strings(e)

	// 检查重复和空的项目
	for i := 0; i < len(e); i++ {
		if (i > 0 && e[i-1] == e[i]) || e[i] == "" {
			continue
		}
		ret = append(ret, e[i])
	}

	// 返回
	return
}
