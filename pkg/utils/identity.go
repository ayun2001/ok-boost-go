package utils

import "github.com/bwmarrin/snowflake"

var snowflakeNode, _ = snowflake.NewNode(CreateRand().Int63n(-1 ^ (-1 << snowflake.NodeBits)))

func GetRandIdInt64() int64 {
	return snowflakeNode.Generate().Int64()
}

func GetRandIdBytes() []byte {
	return StringToBytes(snowflakeNode.Generate().String())
}

func GetRandIdString() string {
	return snowflakeNode.Generate().String()
}

func GetRandIdBase64() string {
	return snowflakeNode.Generate().Base64()
}

func GetRandIdIntBytes() [8]byte {
	return snowflakeNode.Generate().IntBytes()
}
