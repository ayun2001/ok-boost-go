package utils

import "strings"

const (
	ConstDefaultPrometheusNamespace     = "okboost"
	ConstDefaultPrometheusSubsystemName = ""
)

var ApplicationName = "default"

func SetApplicationName(name string) {
	ApplicationName = strings.TrimSpace(name)
}
