package utils

import (
	"math/rand"
	"time"
)

const letters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func CreateRand() *rand.Rand {
	return rand.New(rand.NewSource(time.Now().UnixNano()))
}

// CreateRandomBytes 用于创建随机字节切片
func CreateRandomBytes(n uint32) []byte {
	bytes := make([]byte, n)
	if n > 0 {
		r := CreateRand()
		if _, err := r.Read(bytes); err != nil { // 检查是否有错误发生
			return make([]byte, 0)
		}
		for i := 0; i < len(bytes); i++ { // 使用 for 循环代替 range，能够更高效地迭代切片
			bytes[i] = letters[bytes[i]%byte(len(letters))] // 随机化字节数组
		}
	}
	return bytes
}

func CreateRandomString(n uint32) string {
	return BytesToString(CreateRandomBytes(n))
}
