package utils

import (
	"math"
	"time"
)

// TruncateDecimal4 uint 代表小数位数，格式位 0.000001 如果是几位就指定为几位
func TruncateDecimal4(value float64) float64 {
	return TruncateDecimal(value, 4)
}

func TruncateDecimal(value float64, n uint16) float64 {
	i := math.Pow10(int(n))
	return math.Round(value*i) / i
}

func TimeDurationToInt64(duration time.Duration) int64 {
	return int64(duration)
}

func Int64ToTimeDuration(value int64) time.Duration {
	return time.Duration(value)
}
