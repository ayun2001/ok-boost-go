package utils

import (
	"bytes"
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
)

func CustomCobraUsage() func(*cobra.Command) error {
	return func(c *cobra.Command) error {
		buf := bytes.Buffer{}
		fmt.Fprintln(&buf, "Usage:")
		fmt.Fprintf(&buf, "\t%s\n", c.UseLine())

		if c.HasAvailableSubCommands() {
			fmt.Fprintln(&buf, "\nAvailable Commands:")
			for _, subCommand := range c.Commands() {
				fmt.Fprintf(&buf, "\t%-32s %s\n", subCommand.Name(), subCommand.Short)
			}
		}

		if c.HasAvailableFlags() {
			fmt.Fprintln(&buf, "\nFlags:")
			c.Flags().VisitAll(func(f *pflag.Flag) {
				fmt.Fprintf(&buf, "\t%-32s %s\n", "-"+f.Shorthand+", --"+f.Name, f.Usage)
			})
		}

		if c.HasExample() {
			fmt.Fprintln(&buf, "\nExamples:")
			fmt.Fprintf(&buf, "\t%s\n", c.Example)
		}

		fmt.Print(BytesToString(buf.Bytes()))
		return nil
	}
}

func CustomCobraHelp() func(*cobra.Command, []string) {
	return func(c *cobra.Command, args []string) {
		ok, err := c.Flags().GetBool("help")
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}
		if ok {
			_ = c.Usage()
			os.Exit(0)
		}
	}
}

func PrettyCobraHelpAndUsage(c *cobra.Command) {
	c.SetUsageFunc(CustomCobraUsage())
	c.SetHelpFunc(CustomCobraHelp())
}
