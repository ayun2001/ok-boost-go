package prometheus

import (
	"sort"

	"gitee.com/ayun2001/ok-boost-go/pkg/pool/bytes"
	"gitee.com/ayun2001/ok-boost-go/pkg/utils"
	"github.com/prometheus/client_golang/prometheus"
)

var bufferPool = bytes.NewBufferPool(0)

func ConvertMapToKeys(labels map[string]string) []string {
	keys := make([]string, 0, len(labels))
	for k := range labels {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	return keys
}

func ConvertLabelsToKeys(labels prometheus.Labels) []string {
	return ConvertMapToKeys(labels)
}

func ConvertMapToString(labels map[string]string) string {
	var buff = bufferPool.Get()
	defer bufferPool.Put(buff)

	keys := make([]string, 0, len(labels))
	for k := range labels {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	for _, k := range keys {
		buff.WriteString("#" + k + ":" + labels[k])
	}

	return utils.BytesToString(buff.Bytes())
}

func ConvertLabelsToString(labels prometheus.Labels) string {
	return ConvertMapToString(labels)
}
