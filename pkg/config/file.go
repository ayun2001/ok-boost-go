package config

import (
	"strings"

	"github.com/spf13/viper"
)

type ConfigType string

func (t ConfigType) String() string {
	return string(t)
}

const (
	ConstConfigTypeYaml ConfigType = "yaml"
	ConstConfigTypeJson ConfigType = "json"
	ConstConfigTypeToml ConfigType = "toml"
)

// LoadConfigFromFile 从文件中读取配置
func LoadConfigFromFile(fn string, configType ConfigType, value interface{}, opts ...viper.DecoderConfigOption) error {
	var err error

	c := viper.New()
	c.SetConfigFile(strings.TrimSpace(fn))                //设置读取的文件名
	c.SetConfigType(validConfigType(configType).String()) //设置文件的类型

	//尝试进行配置读取
	if err = c.ReadInConfig(); err != nil {
		return err
	}

	if err = c.Unmarshal(value, opts...); err != nil {
		return err
	}

	return nil
}

// validConfigType 验证配置文件类型
func validConfigType(configType ConfigType) ConfigType {
	switch configType {
	case ConstConfigTypeYaml, ConstConfigTypeJson, ConstConfigTypeToml:
		return configType
	default:
		return ConstConfigTypeYaml
	}
}
